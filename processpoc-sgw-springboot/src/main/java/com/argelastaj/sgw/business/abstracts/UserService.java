package com.argelastaj.sgw.business.abstracts;

import java.util.List;

import com.argelastaj.sgw.entities.UserMySQL;

public interface UserService {

	
	public List<UserMySQL> getAll();
	public UserMySQL getById(int id);
	public void addUser(UserMySQL userMySQL);
	
}
