package com.argelastaj.sgw.business.concretes;

import java.util.List;
import com.argelastaj.sgw.entities.UserMySQL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.argelastaj.sgw.business.abstracts.UserService;
import com.argelastaj.sgw.dataAccess.abstracts.UserDao;


@Service
public class UserManager implements UserService {

	
	private UserDao userDao;
	
	@Autowired
	public UserManager(UserDao userDao) {
		this.userDao=userDao;
	}
	
	@Override
	public List<UserMySQL> getAll() {
		
		return this.userDao.findAll();
	}

	@Override
	public UserMySQL getById(int id) {
		return this.userDao.getById(id);
	}

	@Override
	public void addUser(UserMySQL userMySQL) {
		this.userDao.save(userMySQL);
		
	}

}
