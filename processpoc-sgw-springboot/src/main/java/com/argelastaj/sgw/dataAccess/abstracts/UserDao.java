package com.argelastaj.sgw.dataAccess.abstracts;

import com.argelastaj.sgw.entities.UserMySQL;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserDao extends JpaRepository<UserMySQL, Integer>{

}
