package com.argelastaj.sgw.dataAccess.concretes;

import com.argelastaj.sgw.dataAccess.abstracts.HBaseDao;
import com.argelastaj.sgw.entities.UserHBase;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;

import java.util.List;

@Service
public class HbaseUserRepository implements HBaseDao {

    @Override
    public List<UserHBase> getAll(Connection connection, String tableName) throws IOException {
        byte[] Name = Bytes.toBytes("name");
        byte[] Surname = Bytes.toBytes("surname");
        byte[] Gender = Bytes.toBytes("gender");
        byte[] Age = Bytes.toBytes("age");
        byte[] CF_INFO = Bytes.toBytes("personal_info");
        List<UserHBase> userList= new ArrayList<>();
        Table table=connection.getTable(TableName.valueOf("user"));
        Scan scan1 =new Scan();
        ResultScanner scanner1=table.getScanner(scan1);



        for(Result res : scanner1) { //tüm satır ve sütunlardaki veriyi tarayıp getirecek
            String name=Bytes.toString(res.getValue(CF_INFO, Name));
            String surname =Bytes.toString(res.getValue(CF_INFO, Surname));
            String gender= Bytes.toString(res.getValue(CF_INFO, Gender));
            int age= Integer.valueOf(Bytes.toString(res.getValue(CF_INFO, Age)));
            String row = Bytes.toString(res.getRow());
            userList.add(new UserHBase(name,surname,age,gender,row));
            System.out.println(Bytes.toString(res.getRow()));
            System.out.println(name);
            System.out.println(surname);
            System.out.println(gender);
            System.out.println(age);

        }
            scanner1.close();



        System.out.println("End of getRows...");
        return userList;
    }

    @Override
    public String getRows(Connection connection, String tableName, String rowkey) {
        return null;
    }

    @Override
    public void deleteRows(Connection connection, String tableName, String rowkey) throws IOException {
        Table table=connection.getTable(TableName.valueOf(tableName));
        Delete delete = new Delete(Bytes.toBytes(rowkey));
            table.delete(delete);
    }
}
