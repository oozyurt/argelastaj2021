package com.argelastaj.sgw.rabbit.producer.concrete;

import com.argelastaj.sgw.entities.UserHBase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Base64;


@Service
public class UserProducer {
    @Value("${sr.rabbit.routing.name}")
    private String routingName;
    @Value("${sr.rabbit.exchange.name}")
    private String exchangeName;


    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    public UserProducer(RabbitTemplate rabbitTemplate){
        this.rabbitTemplate=rabbitTemplate;
    }


       public void sendToQueue(UserHBase user){
        System.out.println("Notification send id: "+user.getName());
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonstr = gson.toJson(user);
        rabbitTemplate.convertAndSend(exchangeName,routingName,jsonstr);


    }

    public static String serializeObjectToString(Serializable o) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(baos);
            oos.writeObject(o);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Base64.getEncoder().encodeToString(baos.toByteArray());
    }
}
