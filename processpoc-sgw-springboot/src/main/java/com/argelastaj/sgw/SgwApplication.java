package com.argelastaj.sgw;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.argelastaj.sgw.rabbit.producer.concrete.UserProducer;
import com.argelastaj.sgw.configuration.HBaseConfig;
import com.argelastaj.sgw.dataAccess.abstracts.HBaseDao;
import com.argelastaj.sgw.entities.UserHBase;
import com.argelastaj.sgw.entities.UserMySQL;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.argelastaj.sgw.business.abstracts.UserService;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class SgwApplication implements CommandLineRunner {
 
   @Autowired
   private UserService userService;
   @Autowired
   private HBaseDao hBaseDao;
   @Autowired
   HBaseConfig hBaseConfig;
   @Autowired
   UserProducer userProducer;
   

  /* @Autowired
   public SgwApplication(UserService userService, @Qualifier("hbaseUserRepository") HBaseDao hBaseDao,RabbitTemplate rabbitTemplate){
       this.userService=userService;
       this.hBaseDao=hBaseDao;
       this.rabbitTemplate=rabbitTemplate;

   }*/

	
    public static void main(String[] args) {
    	
        SpringApplication.run(SgwApplication.class, args);




        
    }
    @Override
    public void run(String... args) throws Exception {
     System.out.println("Running Spring Boot Application");
      
     
     List<UserHBase> usrList =new ArrayList<>();
     for(;;) {
    	 try {
             usrList=hBaseDao.getAll(hBaseConfig.createConnection(),"user");
         } catch (IOException e) {
             System.out.println(e.getMessage());;
         }for (UserHBase userHBase : usrList){
             UserMySQL user = new UserMySQL(userHBase.getName(),userHBase.getSurname(), userHBase.getAge(),userHBase.getGender());
             userService.addUser(user);
             userProducer.sendToQueue(userHBase);
             try {
                 hBaseDao.deleteRows(hBaseConfig.createConnection(), "user", userHBase.getUuid());
             }catch (Exception e)
             {
                 System.out.println(e.getMessage());
             }
             System.out.println(user.toString());

         }Thread.sleep(3000);

     }}}



