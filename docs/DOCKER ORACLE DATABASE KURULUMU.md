## DOCKER ORACLE DATABASE KURULUMU

#### 1.ADIM: GEREKLİ ARAÇLARIN İNDİRİLMESİ

***Not:*** Oracle database bu işlem için yüksek bellek kapasitesine ihtiyaç duymaktadır. **Bkz.** [Sanal makine RAM ve işlemci kullanım limitlemesi](https://itnext.io/wsl2-tips-limit-cpu-memory-when-using-docker-c022535faf6f)

* [Github Sitesi Docker imges projesini indirin.](https://github.com/oracle/docker-images.git) 

* Projede Oracle19c standart edition 2 kullanılmıştır. Bu linki kullanarak indirmelisiniz. [Oracle DB 19c](https://www.oracle.com/tr/database/technologies/oracle19c-linux-downloads.html#license-lightbox")

   

![img](https://cdn.discordapp.com/attachments/861539670293479430/864860685095141426/unknown.png)

* İndirdiğimiz Oracle kurulumunu resimde görüldüğü gibi Github projesine yerleştirelim. 



![img](https://cdn.discordapp.com/attachments/861539670293479430/864861457958305802/unknown.png)



![img](https://cdn.discordapp.com/attachments/861539670293479430/864861923657121802/sh.PNG)



* WSL ile oluşturdğumuz Linux konsolunda buildContainerImage.sh dosyasının bulunduğu dizine gelip aşağıdaki kodu uygulayın.

  

  ```bash
  ./buildContainerImage.sh -v 19.3.0 -e
  ```

* İşlem tamamlandıktan sonra konsola aşağıdaki kodu uygulayın.

​     

![img](https://cdn.discordapp.com/attachments/861539670293479430/864863372301828136/volum.PNG)



```bash
docker volume create oracle-db
```

#### DOCKER NETWORK KURULUMU

* Docker da containerların biribirleriyle haberleşebilmesi için network yapısını oluşturuyoruz. 

```bash
docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 multi-host-network
```



* Bu işlemi tamamladıktan sonra Docker container oluşturmak aşağıdaki kodu uygulayın.

  ```bash
  docker run -d --name oracledb --ip 172.20.128.3 --network multi-host-network -p 1521:1521 -p 5500:5500 -e ORACLE_SID=test -e ORACLE_PWD=1234 -v oracle-db:/opt/oracle/oradata oracle/database:19.3.0-ee
  ```
  
  ***Not:*** Oracle Sql de partitioning yapısı enterprise edition da aktif olarak gelmektedir.

![img](https://cdn.discordapp.com/attachments/861539670293479430/864863781514772480/run.PNG)

**ÖRNEK KULLANIMLAR:**

```bash
docker run --name <container name> \
-p <host port>:1521 -p <host port>:5500 \
-e ORACLE_SID=<your SID> \
-e ORACLE_PDB=<your PDB name> \
-e ORACLE_PWD=<your database passwords> \
-e INIT_SGA_SIZE=<your database SGA memory in MB> \
-e INIT_PGA_SIZE=<your database PGA memory in MB> \
-e ORACLE_EDITION=<your database edition> \
-e ORACLE_CHARACTERSET=<your character set> \
-e ENABLE_ARCHIVELOG=true \
-v [<host mount point>:]/opt/oracle/oradata \
oracle/database:19.3.0-ee

Parameters:
   --name:        The name of the container (default: auto generated).
   -p:            The port mapping of the host port to the container port.
                  Two ports are exposed: 1521 (Oracle Listener), 5500 (OEM Express).
   -e ORACLE_SID: The Oracle Database SID that should be used (default: ORCLCDB).
   -e ORACLE_PDB: The Oracle Database PDB name that should be used (default: ORCLPDB1).
   -e ORACLE_PWD: The Oracle Database SYS, SYSTEM and PDB_ADMIN password (default: auto generated).
   -e INIT_SGA_SIZE:
                  The total memory in MB that should be used for all SGA components (optional).
                  Supported 19.3 onwards.
   -e INIT_PGA_SIZE:
                  The target aggregate PGA memory in MB that should be used for all server processes attached to the instance (optional).
                  Supported 19.3 onwards.
   -e ORACLE_EDITION:
                  The Oracle Database Edition (enterprise/standard).
                  Supported 19.3 onwards.
   -e ORACLE_CHARACTERSET:
                  The character set to use when creating the database (default: AL32UTF8).
   -e ENABLE_ARCHIVELOG:
                  To enable archive log mode when creating the database (default: false).
                  Supported 19.3 onwards.
   -v /opt/oracle/oradata
                  The data volume to use for the database.
                  Has to be writable by the Unix "oracle" (uid: 54321) user inside the container!
                  If omitted the database will not be persisted over container recreation.
   -v /opt/oracle/scripts/startup | /docker-entrypoint-initdb.d/startup
                  Optional: A volume with custom scripts to be run after database startup.
                  For further details see the "Running scripts after setup and on startup" section below.
   -v /opt/oracle/scripts/setup | /docker-entrypoint-initdb.d/setup
                  Optional: A volume with custom scripts to be run after database setup.
                  For further details see the "Running scripts after setup and on startup" section below.
```



* **docker run:** Docker'da imageların çalışmasını sağlar.

* **-d:** Docker containerın arka planda çalışmasını sağlar.

* **-p:** Docker ortamındaki portları yerel bilgisayarımızda kullanmamızı sağlar. 

  Örnek kullanımı: **(-p {yerel port}:{docker port})**

* OracleDB arayüzü 1521 portunu kullanarak haberleşmektedir.

* **-v:** Kalıcı kalıp oluşturuyoruz(volume).

* **-name:** Docker'da bu tag, containerın ismini manuel olarak vermemizi sağlar.

  Docker'da kullandığımız image:  **oracle/database:19.3.0-se2**

* ***Not:*** Image'ın yanına versiyon belirtmediğimiz zaman Docker otomatik olarak en son versiyonu"latest" indirecektir.

* **-e:** Ortam değişkenleri parametlerini belirlemek için kullanılır.



* Bu komutu uyguladıktan sonra Oracle database kendini kurmaya başlayacaktır. 



![img](https://cdn.discordapp.com/attachments/861539670293479430/864797825547239444/unknown.png)



* Docker loglarında database hazır olduğu bilgisi yayınlanmaktadır. Dilersek Docker konsol komutları ve SQL plus aracılığıyla database bağlantısı sağlayabiliriz. 

```bash
docker exec -it oracle /bin/bash
```



* Sonrasında gelen komut satırına 

```bash
sqlplus sys@test as sysdba  
```



![img](https://cdn.discordapp.com/attachments/861539670293479430/864867947751407647/unknown.png)



**ÖRNEK KULLANIMLAR:**

```bash
sqlplus sys/<your password>@//localhost:1521/<your SID> as sysdba
sqlplus system/<your password>@//localhost:1521/<your SID>
sqlplus pdbadmin/<your password>@//localhost:1521/<Your PDB name>
```



* SQL plus docker run komutunda oluşturmuş olduğumuz şifreyi isteyecektir. Şifremizi girip SQL komutlarını uygulayabiliriz.



![img](https://cdn.discordapp.com/attachments/861539670293479430/864868037512396810/unknown.png)

#### REFERANSLAR:
* [Github linki](https://github.com/oracle/docker-images.git)
* [Docker Oracle Database kurulum video](https://www.youtube.com/watch?v=OlRYjLBSujY)
* [Oracle Database linki](https://www.oracle.com/tr/database/technologies/oracle19c-linux-downloads.html#license-lightbox)
* [Sanal makine RAM ve işlemci kullanım limitlemesi](https://itnext.io/wsl2-tips-limit-cpu-memory-when-using-docker-c022535faf6f)

