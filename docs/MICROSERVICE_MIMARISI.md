# Microservice Mimarisi

* Microserviceler temelde bir yazılım uygulamasında belirli özellik yada fonksiyonu sağlayan, tek bir amaca hizmet eden, birbirinden bağımsız yazılım servislerdir. Bu hizmetlerin bağımsız olarak bakımı yapılabilir, izlenebilir ve dağıtılabilir yapıya sahip olmalıdır.

* Kısaca **Microservice** tek başına tek sorumluluğu olan ve tek iş yapan sadece o işe ait işleri yürüten modüler projelerdir.

![img](https://cdn.discordapp.com/attachments/861539670293479433/879707137897533480/Microservice_Architecture.png)

## Microservicelerin Temel Özellikleri

- **Bağımsız Deployment** : Servisler birbirinden bağımsız olarak, herhangi bir platformda ayrı ayrı deploy edilebilir.  Her bir servis bağımsız olarak geliştirilebilmeli, test edilebilmeli, deploy edilebilmelidir. Böylece uygulamanın kalitesi ve esnekliği artar.

- **Bağımsız Geliştirme** : Tüm microservice kendi işlevselliğine göre kolayca geliştirilebilir. Herhangi bir servisteki sorun, diğer servisleri etkilemez, böylece kullanıcılar diğer servisleri kullanabilir konumda olur.

- **Hata İzolasyonu** : Projenin herhangi bir servisinde oluşacak bir sorun, projenin diğer servislerini etkilemeyeceği için, sistem hala çalışmaya devam eder.

- **Ölçeklenebilir** : Her bir bileşen kendi ihtiyacına göre ölçeklenebilir, tüm bileşenleri ölçeklendirmeye gerek yoktur. Herhangi bir serviste oluşacak trafik tüm sistemin scale olmasını gerektirmeden, trafik altındaki servis çoklanarak sorun ortadan kaldırılabilir.

- **Teknoloji Çeşitliliği** : Tüm servisler, alanlarına, görevlerine ve özelliklerine göre farklı microservicelere ayrılmalıdır. Aynı projenin farklı servislerini geliştirirken farklı diller ve teknolojiler kullanılabilir.

- Tüm microserviceler birbirleriyle REST veya Message Bus üzerinden haberleşmelidir. İkisi birden de kullanılabilir.

- Microserviceler tarafından gerçekleştirilen tüm işlevler API Gateway üzerinden istemcilere iletilebilir olmalıdır. Tüm iç end-pointler API Gateway’e bağlanır. Böylece, API Gateway’e bağlanan herhangi biri otomatik olarak tüm sisteme bağlanabilmelidir.

  **Bağımsızlık** microservicelerin temelini oluşturan en önemli etmen olarak göze çarpmaktadır. Bağımsız geliştirme, bağımsız programlama dili, bağımsız veritabanı, bağımsız teknoloji, bağımsız test edilebilme, bağımsız deploy edilebilmeli özellikleri microservice temelini oluşturmaktadır.

## Ne Tür Projeler Microservice Mimarisine Uygundur ?

Çoğu şirket yeni bir projeye monolitik mimariyle başlarlar. Projelerin ilk aşamasında, monolit bir yapı kurmak ve işlerin daha hızlı ilerlemesini sağlamak çok daha kolay bir yoldur. Bir süre sonra, eski kod yapıları, yeni teknolojilere adapte olamamak, kod karmaşıklığının yada büyüklüğünün getirdiği yavaşlık, anlaşılırlığın azalması, yeni özellik eklenmesinin zorluğu gibi problemlerle karşılaşırlar . Sistemin büyümesiyle, kod karmaşık hale gelirken, mimari daha da karmaşık hale gelir ve bunu sürdürmek zorluğun yanında, daha fazla developer ve zaman ihtiyacı doğurur. Bunun yanında, yeni ihtiyaçlara ve özelliklere cevap verilememeye başlanır. Böyle bir projenin modüler olduğunu umut ederek, microservice mimarisine bu problemlerden kurtulmak amacıyla geçilebilir. Ama halihazırdaki monolitik mimari size yetiyorsa, böyle bir değişime gerçekten ihtiyacınız olup olmadığını sorgulanması gerekir.

- Orta ve büyük ölçekli projeler
- Microservice mimari bilgisi bulunan ekip üyeleri (mümkünse yazılım mimarı)
- Birden fazla takıma sahip proje ekipleri (Her biri microservice mimarisini kavramış olmalı)
- Çok modüllü yapılar
- Ölçeklenebilir olması gereken servisler
- Versiyonlama da zero-time hedefleyen projeler
- Büyük trafik altındaki uygulamalar

Özetle, microservice mimarisi, özellikle büyük ölçekli, modüler ve büyük kitlelere hitap eden uygulamalar için oldukça uygundur. Microservices mimari modeli, karmaşık ve sürekli gelişen uygulamalar için daha iyi bir seçimdir. Ancak unutulmamalıdır ki, iyi ve mimariyi bilen kişilerin barındığı bir ekibe sahip olmak en önemli etkenlerden biridir. Kötü bir microservice yapısı, sizi kurtulmak istediğiniz tüm problemlerle karşılaşmanızı sağlayabilir.

![img](https://cdn.discordapp.com/attachments/861539670293479433/879999075414581268/Frame-25.png)

## Microservice'lerde Hangi Teknolojiler Tercih Edilmektedir ?

* Takımlar oluştuktan ve mikroservislerin kağıt üzerindeki tasarımları kabaca bittikten sonra teknoloji seçimi yapılması gerekiyor. Mikroservis mimarilerde servislerin farklı teknolojileri kullanmaları mümkün, hatta önerilmekte. Servislerde farklı teknolojilerin kullanılmasını **Poliglot Mikroservis Tasarımı** diye adlandırabiliriz.

![img](https://cdn.discordapp.com/attachments/861539670293479433/880006930653929472/2-1.png)

* Her bir mikroservis, ilgili olduğu işin gerekliliklerine ve geliştirici takımın yeteneklerine göre farklı bir teknoloji kullanabilir. Farklı teknolojilerin kullanılmasının bir avantajı da şudur ki, firma dışarıdan yapacağı istihdam için illa java’cı veya .net’çi vs. bulmaya kalktığı zaman, **yetenek havuzu** daraltılmış oluyor. Ancak servislerdeki teknoloji çeşitliliği yetenek havuzunun daha geniş olmasını ve istihdamın daha kolay yapılmasını sağlayacaktır.
* Microserviceler teknoloji kullanımı konusunda bağımsız olduklarından dolayı her servis farklı bir teknoloji kullanabilir. Bu yüzden takımları teknoloji seçiminde serbest bırakmak geliştiricilerin teknik manada yaratıcılığını destekleyecektir. Takım daha rahat olduğu programlama dilini seçecektir. Takımda otonomi artacak ve takım sorumlu olduğu servisleri için bağımsız bir şekilde geliştirme, yaygınlaştırma ve ölçekleme yapabilecektir. Lakin ipleri tamamen takımların eline bırakmak bir zaman sonra birbiri ile hiç anlaşamayan servislerin ve takımların ortaya çıkmasına neden olabilir.

![img](https://cdn.discordapp.com/attachments/861539670293479433/880008827070726194/3.png)

* **Bu repoda microservicelerde kullanılan teknolojilerin çoğu açıklamalarıyla birlikte paylaşılmıştır, incelemenizi öneririm.** https://github.com/mfornos/awesome-microservices#haskell

## Referanslar

* https://architecht.com/blog/?p=264
* https://microservices.io/
* https://gokhana.medium.com/microservice-mimarisi-nedir-microservice-mimarisine-giri%C5%9F-948e30cf65b1
* https://gokhana.medium.com/microservice-mimarisi-hangi-uygulamalar-i%CC%87%C3%A7in-uygundur-da8e80668eb4