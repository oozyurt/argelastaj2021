## WINDOWS DOCKER MYSQL AYAKLANDIRILMASI ##

#### DOCKER MYSQL KOMUTUNUN UYGULANMASI ####

```powershell
docker run --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=1234 -d mysql:8.0
```



![img](https://cdn.discordapp.com/attachments/861539670293479430/863053642139435058/mysql.PNG)



* **docker run:** Docker'da imageların çalışmasını sağlar.

* **-d:** Docker containerın arka planda çalışmasını sağlar.

* **-p:** Docker ortamındaki portları yerel bilgisayarımızda kullanmamızı sağlar. 

  Örnek kullanımı: **(-p {yerel port}:{docker port})**

*  Mysql, 3306 portunu kullanmaktadır.

* **-name:** Docker'da bu tag, containerın ismini manuel olarak vermemizi sağlar.

   Docker'da kullandığımız image:  **mysql:8.0**

* ***Not:*** Image'ın yanına versiyon belirtmediğimiz zaman Docker otomatik olarak en son versiyonu"latest" indirecektir.

* **-e:** Ortam değişkenleri parametlerini belirlemek için kullanılır.

  ***Not:*** Mysql parametre olarak root_password istemektedir. Bunun için ortam parametrelerine bu değişken girilmelidir. 

  Örnek: MYSQL_ROOT_PASSWORD=1234

  

