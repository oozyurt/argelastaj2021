## Oracle Database Partitioning 

##### Ön Koşul:

  * Oracle Database Enterprise Edition olmalı.

#### Partition yapısı

  Oracle database partition teknolojisi basit tabir ile çok büyük tabloların ayrı ayrı bölümler haline dönüştürmemizi sağlayan yapıdır. 

![img](https://www.farukcevik.com.tr/wp-content/uploads/2020/05/image.png)

**Partition Türleri**

  * **Single Partitioning  **

    * List Partitioning

    * Range Partitioning

    * Hash Partitioning
    
      ![ORACLE PARTITION KULLANIMI |(Single Partitioning Examples)](https://www.farukcevik.com.tr/wp-content/uploads/2020/05/image-1.png)
  * **Composite Partitioning**



![ORACLE PARTITION KULLANIMI |(Composite Partitioning Examples)](https://alperkurak.files.wordpress.com/2016/08/oracle173.gif)

**RANGE** **PARTITIONING**: Belirli bir aralığa göre partition yapısını kurar (genelde tarih aralığıdır).Her partition için ayrı olmakla beraber,belirli bir kolondaki değerlere göre yapılır.Aynı zamanda en çok kullanılan partition yapısıdır.

```sql
CREATE TABLE invoices
(invoice_no    NUMBER NOT NULL,
 invoice_date  DATE   NOT NULL,
 comments      VARCHAR2(500))
PARTITION BY RANGE (invoice_date)
(PARTITION invoices_q1 VALUES LESS THAN (TO_DATE('01/04/2011', 'DD/MM/YYYY')) TABLESPACE users,
 PARTITION invoices_q2 VALUES LESS THAN (TO_DATE('01/07/2011', 'DD/MM/YYYY')) TABLESPACE users,
 PARTITION invoices_q3 VALUES LESS THAN (TO_DATE('01/09/2011', 'DD/MM/YYYY')) TABLESPACE users,
 PARTITION invoices_q4 VALUES LESS THAN (TO_DATE('01/01/2012', 'DD/MM/YYYY')) 
);
```



**LIST PARTITIONING** : Range’te bir sınır değerler baz alınıyorken list yönteminde partiton key’in verilen bir listede var olup olmaması baz alınır. Örneğin tablomuzda Ülke bilgisi tutan bir kolon olsun. Bu kolon değerini partiton key seçtiğinizde ilgili ükede olan kayıtların o partitionlara eknemesi sağlanabilir.

```sql
CREATE TABLE orders
(
  id            NUMBER,
  country_code  VARCHAR2(5),
  customer_id   NUMBER,
  order_date    DATE,
  order_total   NUMBER(8,2),
  CONSTRAINT orders_pk PRIMARY KEY (id)
)
PARTITION BY LIST (country_code)
(
  PARTITION part_usa VALUES ('USA'),
  PARTITION part_uk_and_ireland VALUES ('GBR', 'IRL')
);

INSERT INTO orders VALUES (1, 'USA', 10, SYSDATE, 10200.93);
INSERT INTO orders VALUES (2, 'USA', 11, SYSDATE, 948.22);
INSERT INTO orders VALUES (3, 'GBR', 22, SYSDATE, 300.83);
INSERT INTO orders VALUES (4, 'IRL', 43, SYSDATE, 978.43);
COMMIT;
```



Ayrıca 12c ile Automatic list partition özelliği ile partition key de olmayan bir alan geldiğinde otomatik olarak partition eklenmesi özelliği de getirilmiştir. Daha önce listede olmayan bir alan geldiğinde hata verip o kaydı veritabanına eklemiyordu ancak artık bu sorun olmaktan çıktı, Bunun için.

```sql
ALTER TABLE orders SET PARTITIONING AUTOMATIC;

--veya tablo create edilirken

CREATE TABLE orders
(
  id            NUMBER,
  country_code  VARCHAR2(5),
  customer_id   NUMBER,
  order_date    DATE,
  order_total   NUMBER(8,2),
  CONSTRAINT orders_pk PRIMARY KEY (id)
)
PARTITION BY LIST (country_code) AUTOMATIC
(
  PARTITION part_usa VALUES ('USA'),
  PARTITION part_uk_and_ireland VALUES ('GBR', 'IRL')
);
```

**HASH PARTITIONING** : Range ve list partitiong’ in kullanılamadığı yani belirli bir tarih veya aralık içermeyen tablolarda dağılım yapılırken baz alınabilecek bir kolon üzerinden oracle hash algortimasını kullanarak tabloları partition ladığımız yöntemdir. Hash Partitioning verinin device’lar arasında dağıtılması için ideal bir yöntemdir. Range partitioning in bir alternatifidir fakat burada tarihsel bir data yoktur. Hash partitioning kullanırken hash algoritmasını sonradan değiştiremezsiniz, partitionları sonradan ayıramaz, merge edmez ve drop edemezsiniz. Yalnızca bazı paritionları birleştirebilirsiniz.

```sql
CREATE TABLE invoices
(invoice_no    NUMBER NOT NULL,
 invoice_date  DATE   NOT NULL,
 comments      VARCHAR2(500))
PARTITION BY HASH (invoice_no)
(PARTITION invoices_q1 TABLESPACE users,
 PARTITION invoices_q2 TABLESPACE users,
 PARTITION invoices_q3 TABLESPACE users,
 PARTITION invoices_q4 TABLESPACE users);
```

**COMPOSITE PARTITIONING**: Yukarıdaki partitioning yöntemlerinin birlikte kullanılması ile oluşturulan partition lara denir.

- Composite Range-Range Partitioning
- Composite Range-Hash Partitioning
- Composite Range-List Partitioning
- Composite List-Range Partitioning
- Composite List-Hash Partitioning
- Composite List-List Partitioning
- Composite Hash-Hash Partitioning
- Composite Hash-List Partitioning
- Composite Hash-Range Partitioning

```
CREATE TABLE invoices
(invoice_no    NUMBER NOT NULL,
 invoice_date  DATE   NOT NULL,
 comments      VARCHAR2(500))
PARTITION BY RANGE (invoice_date)
SUBPARTITION BY HASH (invoice_no)
SUBPARTITIONS 8
(PARTITION invoices_q1 VALUES LESS THAN (TO_DATE('01/04/2001', 'DD/MM/YYYY')),
 PARTITION invoices_q2 VALUES LESS THAN (TO_DATE('01/07/2001', 'DD/MM/YYYY')),
 PARTITION invoices_q3 VALUES LESS THAN (TO_DATE('01/09/2001', 'DD/MM/YYYY')),
 PARTITION invoices_q4 VALUES LESS THAN (TO_DATE('01/01/2002', 'DD/MM/YYYY'));
```

**INTERVAL PARTITIONING** :  Aşağıdaki gibi partitionları olan bir tablomuz olduğunu düşünelim.

```sql
CREATE TABLE invoices
(invoice_no    NUMBER NOT NULL,
 invoice_date  DATE   NOT NULL,
 comments      VARCHAR2(500))
PARTITION BY RANGE (invoice_date)
(PARTITION invoices_q1 VALUES LESS THAN (TO_DATE('01/04/2020', 'DD/MM/YYYY')) TABLESPACE users,
 PARTITION invoices_q2 VALUES LESS THAN (TO_DATE('01/07/2020', 'DD/MM/YYYY')) TABLESPACE users,
 PARTITION invoices_q3 VALUES LESS THAN (TO_DATE('01/09/2020', 'DD/MM/YYYY')) TABLESPACE users,
 PARTITION invoices_q4 VALUES LESS THAN (TO_DATE('01/01/2021', 'DD/MM/YYYY')) 
);
```

Bu tabloya 02/02/2021 tarihli bir kayıt eklemek istediğimizde bize

```sql
ORA-14400: inserted partition key does not map to any partition
```

Hatasını verecektir. Çünkü bu tarih aralığı için partition oluşturmamışız. İşte tam bu noktada Interval partition tanımlanması durumunda bu partition yoksa oracle bizim için bu partition’u otomatik oluşturacak ve içerisine ilgili veriyi insert edecektir. Interval partition nasıl oluşturulur. Bu rada ilk partition’u mutlaka biz oluşturmalıyız sonraki partition’ları oracle kendisi oluşturacaktır

```sql
CREATE TABLE invoices
(invoice_no    NUMBER NOT NULL,
 invoice_date  DATE   NOT NULL,
 comments      VARCHAR2(500))
PARTITION BY RANGE (invoice_date)
INTERVAL (NUMTOYMINTERVAL(1,'MONTH'))
(PARTITION invoices_q1 VALUES LESS THAN (TO_DATE('01/04/2020', 'DD/MM/YYYY')) TABLESPACE users 
);
```

Aşağıda örnek bir RANGE-LIST Compozit Partition var aynı zamanda 7 günlük range yapacak şekilde INTERVAL özellikte verilmiş durumda. Bu gibi senaryolar ihtiyaca göre çoğaltılabilir sizin sorgunuz ve tablo yapınıza göre partition şekli değişecektir.

```sql
CREATE TABLE TBL_OZET_T1
(
  Id                      NUMBER(10),
  STATION_ID                NUMBER(10),
  tarih                   DATE,
  Enerji                  BINARY_FLOAT,
  aylikEnerji             BINARY_FLOAT,
  yillikEnerji            BINARY_FLOAT,
  toplamEnerji            BINARY_FLOAT,
  isinim                  BINARY_FLOAT,
  sicaklik                BINARY_DOUBLE,
  gunlukUretim            BINARY_FLOAT,
  hucreSicakligi          BINARY_FLOAT,
  ruzgarHizi              BINARY_FLOAT,
)
PARTITION BY RANGE(tarih) 
INTERVAL (NUMTODSINTERVAL(7,'DAY'))
SUBPARTITION BY range(STATION_ID) 
(PARTITION  P_1 VALUES LESS THAN (TO_DATE('25/04/2021','DD/MM/YY'))
(SUBPARTITION ST_GRP1 VALUES LESS THAN(125),
SUBPARTITION ST_GRP2 VALUES LESS THAN(342),
SUBPARTITION ST_GRP3 VALUES LESS THAN(358),
SUBPARTITION ST_GRP4 VALUES LESS THAN(374),
SUBPARTITION ST_GRP5 VALUES LESS THAN(409),
SUBPARTITION ST_GRP6 VALUES LESS THAN(440),
SUBPARTITION ST_GRP7 VALUES LESS THAN(483),
SUBPARTITION ST_GRP8 VALUES LESS THAN(522),
SUBPARTITION ST_GRP9 VALUES LESS THAN(585)
))
```

Partition olmayan bir tabloyu partitionlu hale getirebilmek için ALTER TABLE tabel_name MODIFY <ONLINE> ….. komutu ile partition lara bölünebilir. Burada dikkat edilemsi gereken 11g de bu özellik yok ve veri tabanı 12c üzeri olmalı.

```sql
ALTER TABLE t1 MODIFY
  PARTITION BY RANGE (created_date) SUBPARTITION BY HASH (id)(
    PARTITION t1_part_2018 VALUES LESS THAN (TO_DATE('01-JAN-2019','DD-MON-YYYY')) (
      SUBPARTITION t1_sub_part_2018_1,
      SUBPARTITION t1_sub_part_2018_2,
      SUBPARTITION t1_sub_part_2018_3,
      SUBPARTITION t1_sub_part_2018_4
    ),
    PARTITION t1_part_2019 VALUES LESS THAN (TO_DATE('01-JAN-2020','DD-MON-YYYY')) (
      SUBPARTITION t1_sub_part_2019_1,
      SUBPARTITION t1_sub_part_2019_2,
      SUBPARTITION t1_sub_part_2019_3,
      SUBPARTITION t1_sub_part_2019_4
    ),
    PARTITION t1_part_2020 VALUES LESS THAN (TO_DATE('01-JAN-2021','DD-MON-YYYY')) (
      SUBPARTITION t1_sub_part_2020_1,
      SUBPARTITION t1_sub_part_2020_2,
      SUBPARTITION t1_sub_part_2020_3,
      SUBPARTITION t1_sub_part_2020_4
    )
  ) ONLINE
  UPDATE INDEXES
  (
    t1_pk GLOBAL,
    t1_created_date_idx LOCAL
  );
```

*****

***Not:***  Insertlerimizde hata almamak için session'da nls_date_format’ ını setleme işlemini gerçekleştirmeliyiz;



```sql
alter session set nls_date_format = 'DD/MM/YYYY' ;
```





##### Referanslar

* https://docs.oracle.com/en/database/oracle/oracle-database/12.2/vldbg/partition-create-tables-indexes.html#GUID-00923EB3-05F6-41F7-8437-E42FC9BD9571
* http://www.mustafabektastepe.com/2017/01/18/oracle-partition/
* http://www.kamilturkyilmaz.com/2012/04/29/interval-partitioning-uzerine/

