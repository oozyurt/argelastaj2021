# Docker Desktop'ı Windows Sisteme Kurulması

[TOC]

### WSL Ortamın Kurulması

Manuel kurulum yapılacaktır.

#####      Adım 1: Windows Subsystem Etkinleştirilmesi

​     Windows da Docker'ı kullanabilmek için Linux Subsystem bağımlılığını kurmamız gerekir.

​     Komut satırını yönetici hakları ile açıp aşağıdaki kodu yazıyoruz.

```powershell
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
```

##### Adım 2: WSL 2'yi çalıştırabilmek için gereksinimlerin kontrol edilmesi

​      WSL  2 Windows 10  64 bit sistemlerde versiyon 1903 ve üzeri sistemlerde çalışmaktadır. Sistemimizin versiyonunu öğrenebilmek için **Windows logo + R** (çalıştır)  açılan pencerede **winver** yazarak kontrol edebiliriz. 

​      

##### Adım 3: Sanal Makine Özelliklerinin etkinleştirilmesi 

​     WSL 2'yi kurmadan önce Sanal Makine özelliklerinin açılması gerekmektedir.

​    Komut satırını yönetici hakları ile açıp aşağıdaki kodu yazıyoruz.

```powershell
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
```

​    Ayarların uygulanabilmesi için cihazın tekrar başlatılması gerekmektedir.



#####      Adım 4: Linux Kernel Güncelleme Paketinin İndirilmesi

​     [Linux Kernel Paketi x64](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi)

​     İndirilen paket bilgisayara kurulmalıdır.

##### Adım 5: WSL 2'nin Varsayılan Versiyon Olarak Ayarlanması

​     WSL 2'yi varsayılan sürüm olarak ayarlamak için bu komutu çalıştırın.

```powershell
wsl --set-default-version 2
```

   

##### Adım 6: Linux Dağıtımının Seçilmesi ve Yüklenmesi

​     [Microsoft Store](https://www.microsoft.com/store/productId/9N9TNGVNDL3Q)'u açın ve Ubuntu dağıtımını indirin.

![img](https://docs.microsoft.com/en-us/windows/wsl/media/store.png)



### WSL Ubuntu Kurulumu

* İndirdiğimiz Linux Dağıtımını kurmaya başlayalım.

* Gelen ekrana yeni Linux kullanıcı adımızı girelim. 

* Daha sonrasında konsol bizden şifre oluşturmamızı isteyecektir. Hatırlayabileceğimiz bir şifre belirleyelim. 

  **Not:** Şifre girilmesi esnasında imleç hareket etmez ama şifreniz girilir.

![Ubuntu unpacking in the Windows console](https://docs.microsoft.com/en-us/windows/wsl/media/ubuntuinstall.png)



### Windows Terminal Kurulumu (İsteğe Bağlı)

[Microsoft Store](https://www.microsoft.com/store/productId/9N0DX20HK701)'dan Windows Terminal'i kurabilirsiniz. Sekmeli yapısı sayesinde Linux ve Powershell arasındaki geçişleri hızlıca gerçekleştirebilirsiniz.



### Docker Kurulumu

 [Docker İndirme Linki](https://desktop.docker.com/win/stable/amd64/Docker%20Desktop%20Installer.exe) ile kurulumu gerçekleştirelim.



![img](https://docs.docker.com/docker-for-windows/images/docker-app-search.png)



* Kurulumdan sonra Docker'ın çalıştığını denemek için komut satırına bu kodları yazalım.

```powershell
docker run hello-world
```





















































