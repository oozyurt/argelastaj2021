### VM ÜZERİNE CENTOS 7 KURULUMU

#### VM KURULUMU

* İlk olarak VirtualBox'ı indiriyoruz ve bilgisayarımıza yüklüyoruz. 

* Site Linki: [Oracle VirtualBox](https://www.virtualbox.org) 

  ![img](https://cdn.discordapp.com/attachments/861539670293479430/874223067595964426/unknown.png)

  

#### CENTOS 7 KURULUMU

* [Centos 7 minimal indirme linki](http://denizli.centos-mirror.guzel.net.tr/7.9.2009/isos/x86_64/CentOS-7-x86_64-Minimal-2009.iso) buradan indiriyoruz.
* Oracle VM'i açıp **"yeni"** tuşuna basarak yeni bir VM yaratıyoruz.
* Adı kısmına centos7min ismini girebilirsiniz.

![img](https://cdn.discordapp.com/attachments/861539670293479430/874219455914340372/unknown.png)

* Bellek boyutunu minimum 4 GB olacak şekilde belirlenmelidir. (Sonrasında arttırabilir.)

  ![img](https://cdn.discordapp.com/attachments/861539670293479430/874219523333574666/unknown.png)

* Sabit Disk ekranında **şimdi sanal bir sabit disk oluştur** seçeneğine tıklayarak ilerleyiniz.

  ![img](https://cdn.discordapp.com/attachments/861539670293479430/874219580418064445/unknown.png)

* Sabit disk dosya türünü VDI olarak seçerek ilerleyiniz.

![unknown.png (456×453)](https://cdn.discordapp.com/attachments/861539670293479430/874219638769192980/unknown.png)

* Dosya yeri ve boyutu ekranına gelindiğinde minimum 45 GB yer ayırarak devam ediniz.

![unknown.png (461×453)](https://cdn.discordapp.com/attachments/861539670293479430/874219762106892348/unknown.png)

* Centos VM'in **ayarlar** sekmesini açınız. Gelen ayarlardan Sistem alanından Anakart sekmesine gidiniz. Disket sürücü inaktif ; Optik, Sabit Disk, Ağ aktif olacak şekilde işaretleyiniz.

![unknown.png (679×531)](https://cdn.discordapp.com/attachments/861539670293479430/874219914502762526/unknown.png)



* Anakart sekmesinin yanında bulunan İşlemci sekmesine giderek işlemci ayarını minimum 2 olarak ayarlayınız.

![unknown.png (676×524)](https://cdn.discordapp.com/attachments/861539670293479430/874219957309820938/unknown.png)

* Ayarlar kısmında **Ekran** alanına giderek, ekran bellek boyutuna 128 MB veriniz.

![unknown.png (671×538)](https://cdn.discordapp.com/attachments/861539670293479430/874220009835081788/unknown.png)



* Ayarlar kısmında **Depolama** alanına gidiniz. İndirmiş olduğumuz ISO image'ını sanal diskinize tanımlayınız.

![unknown.png (900×535)](https://cdn.discordapp.com/attachments/861539670293479430/874220170716000286/unknown.png)

* Ağ ayarların da Bağdaştırıcı 1 sekmesinde **Şuna takıldı** listesinden **köprü bağdaştırıcı**sını seçiniz. Gelişmiş ayarları aktif ediniz. **Karma Kipi**' ni **tümüne izin ver** seçerek tamama tıklayınız.

![unknown.png (674×532)](https://cdn.discordapp.com/attachments/861539670293479430/874220307056037938/unknown.png)



* Oluşturduğumuz VM'i başlatınız. Gelen ekranda onay verip devam ediniz.

![unknown.png (643×556)](https://cdn.discordapp.com/attachments/861539670293479430/874220511926820904/unknown.png)

![unknown.png (805×674)](https://cdn.discordapp.com/attachments/861539670293479430/874220657045561344/unknown.png)



* Türkçe klavye için kurulumda Türkçe klavye desteğini ekliyoruz İngilizce klavye desteğini kaldırıyoruz.

![unknown.png (805×676)](https://cdn.discordapp.com/attachments/861539670293479430/874221140590071828/unknown.png)



* Kurulum yolunu tanımlamak için **INSTALLITON DESTINATION**' a tıklayınız. Gelen ekranda **Done** tuşuna basarak kurulumu onaylayınız.

![unknown.png (805×677)](https://cdn.discordapp.com/attachments/861539670293479430/874221360510009384/unknown.png)

![unknown.png (814×684)](https://cdn.discordapp.com/attachments/861539670293479430/874221439044173844/unknown.png)



* **NETWORK&HOST NAME**'e tıklayarak gelen ekranda off konumundaki Ethernet bağdaştırıcıyı on konumuna getirerek açınız.

![unknown.png (796×654)](https://cdn.discordapp.com/attachments/861539670293479430/874221540034609202/unknown.png)



* **Begin Install** tuşuna bastıktan sonra gelen ekranda root ve user kullanıcı şifresini belirleyebilirsiniz.

![unknown.png (810×682)](https://cdn.discordapp.com/attachments/861539670293479430/874219084546445332/unknown.png)



* Kurulum tamamlandıktan sonra VM restart olur ve terminal login ekranı ile karşılar.

![unknown.png (824×687)](https://cdn.discordapp.com/attachments/861539670293479430/874231381927022652/unknown.png)

