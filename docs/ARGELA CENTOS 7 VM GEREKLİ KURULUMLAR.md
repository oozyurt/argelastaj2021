### ARGELA CENTOS 7 VM GEREKLİ KURULUMLAR

#### JAVA JDK 8 KURULUMU

* Java 8 JDK kurulumunda kullanılacak kod satırları:

```bash
sudo yum update
```

```bash
sudo yum install java-1.8.0-openjdk-devel
```

* Java yüklemesinden sonra Java'nın çalışıp çalışmadığını bu kod ile test edebiliriz.

```bash
java –version
```



#### DOCKER KURULUMU

```bash
sudo yum install -y yum-utils
```

```bash
sudo yum-config-manager --add-repo
https://download.docker.com/linux/centos/docker-ce.repo
```

* Docker Engine yüklüyoruz.

```bash
sudo yum install docker-ce docker-ce-cli containerd.io
```

* Yüklenen Docker'ı çalıştırmak için:

```bash
sudo systemctl start docker
```

* Docker test containerı:

```bash
sudo docker run hello-world
```



##### Sistem başlatıldığında Docker'ın otomatik olarak başlatılması

* Mevcut Linux kullanıcımıza Docker komutlarını uygulayabilmesi için yetki veriyoruz. 

```bash
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
```

* Sistem açıldığında Docker otomatik başlatılacaktır.

```bash
sudo systemctl enable docker.service
sudo systemctl enable containerd.service
```



#### DOCKER NETWORK KURULUMU

* Docker da containerların biribirleriyle haberleşebilmesi için network yapısını oluşturuyoruz. 

```bash
docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 multi-host-network
```



#### DOCKER CONTAINERLARININ KURULUMU

##### WEBLOGIC KURULUMU

* Docker weblogic için paylaşacağımız **"weblogic"** isimli klasörü oluşturuyoruz.

```
mkdir weblogic
```

* Weblogic klasörünün içerisine "domain.properties" isimli dosyayı oluşturuyoruz. Username ve password bilgilerimizi ekliyoruz.

```bash
cd weblogic
nano domain.properties
```

**NOT:** nano metin editörünüz yoksa  **sudo yum install nano** yazarak indirebilirsiniz.

* Konsol üzerinde docker login komutuyla oturum açın.

```bash
docker login container-registry.oracle.com
```

* Oturum başarılı mesajı alındıktan sonra aşağıdaki komutu uygulayarak docker imajını bilgisayarınıza çekin.

```bash
docker pull container-registry.oracle.com/middleware/weblogic:12.2.1.4-dev-ol8
```

```bash
cd weblogic 
ls   //domain.properties dosyasını görmeniz gerekir

docker run -d --name weblogic12 --ip 172.20.128.2 --network multi-host-network -p 7001:7001 -p 9002:9002 -p 7003:7003 -p 7005:7005 -v $PWD:/u01/oracle/properties -v $PWD/user_pts:/u01/oracle/user_projects container-registry.oracle.com/middleware/weblogic:12.2.1.4-dev-ol8
```



##### ADMIN SERVER CONTAINER'I ÜZERİNDE NODE MANAGER VE MANAGED SERVER AYAKLANDIRILMASI

##### 1.YÖNTEM: DOCKER DİZİNİNE ERİŞMEK

* Docker containerları oluşturulurken eşsiz bir isim belirler. Bu klasör yolunu bulabilmek adına aşağıdaki kodu uygulayarak **UpperDir** yolunu takip ederek klasöre ulaşabiliriz. 

```bash
docker inspect weblogic12
```



![unknown.png (1216×244)](https://cdn.discordapp.com/attachments/861539670293479430/874626223177891890/unknown.png)



* Sonrasında bu yola gidebilmek için root kullanıcı yetkisi almalıyız. 

```bash
sudo su  
cd /var/lib/docker/overlay2/2d4846002a74efbbe02603d615c6b138eb2820061b7605cb424d0a79ab71b7a4/diff/u01/oracle/user_projects/domains/base_domain/nodemanager
```



##### 2.YÖNTEM: VOLUME ÜZERİNDEN ERİŞMEK

* Run komutu ile oluşturmuş olduğumuz volume yapısını kullanarak kolayca nodemanager'ın bulunduğu dizine erişebiliriz. 

  ```bash
  cd ./weblogic/user_pts/domains/base_domain/nodemanager
  ```

  

* Belirtilen dizinde nodemanager klasörüne ulaştıktan sonra **nodemanager.properties** dosyasını bir text editörü yardımıyla düzenleyerek **SecureListener** parametresini **true** değerinden **false** değerine getirelim.

```bash
nano nodemanager.properties
```

* nano yoksa **sudo yum install nano** olarak yükleyebilirsiniz.

![unknown.png (1027×143)](https://cdn.discordapp.com/attachments/861539670293479430/874627864237047828/unknown.png)

![unknown.png (569×191)](https://cdn.discordapp.com/attachments/861539670293479430/874627935649267762/unknown.png)



* Node manager'ı ayaklandırmak için aşağıdaki kodu kullanalım.

```bash
docker exec -d weblogic12 ./user_projects/domains/base_domain/bin/startNodeManager.sh
```



##### MACHINE VE MANAGED SERVER OLUŞTURMA

###### MACHINE OLUŞTURMA

* Weblogic de Environment altında **Machines** alanına gidelim. **"New"** tuşuna basarak yeni bir makine oluşturalım. İsim alanına istediğimiz bir ismi girelim.

![unknown.png (370×356)](https://cdn.discordapp.com/attachments/861539670293479430/874629810415108116/unknown.png)

![unknown.png (639×523)](https://cdn.discordapp.com/attachments/861539670293479430/874629959694565436/unknown.png)



* Type alanını **Plain** seçelim. Finish tuşuna basın.

![unknown.png (684×643)](https://cdn.discordapp.com/attachments/861539670293479430/874630021950627900/unknown.png)



###### MANAGED SERVER OLUŞTURMA

* Weblogic de Environment altında **Servers** alanına gidelim. **"New"** tuşuna basarak yeni bir server oluşturalım.
* Docker da açtığımız portlar arasında 7003 olduğu için Server Listen Port değerine 7003 verdik.

![unknown.png (938×592)](https://cdn.discordapp.com/attachments/861539670293479430/874631462484328528/unknown.png)

* Oluşturduğumuz server da, machine seçimini yapalım.

![unknown.png (1197×670)](https://cdn.discordapp.com/attachments/861539670293479430/874632024730779668/unknown.png)



* Machine seçimi yapıldıktan sonra save butonuna basalım. Sonrasında Advanced sekmesini açarak **Local Administration Port Override** parametresini değiştirelim.

![unknown.png (1114×980)](https://cdn.discordapp.com/attachments/861539670293479430/874632063293194251/unknown.png)



* Node manager'ın açık olduğundan emin olun. Node manager'ın ulaşılabilir olduğunu oluşturduğumuz Machine altında Monitoring sekmesinden görebiliriz.

![unknown.png (1822×422)](https://cdn.discordapp.com/attachments/861539670293479430/874633530553028628/unknown.png)



* Servers kısmında Control sekmesine giderek oluşturduğumuz Server'ı ayaklandırabiliriz.

![unknown.png (1579×626)](https://cdn.discordapp.com/attachments/861539670293479430/874633597334720573/unknown.png)

##### ORACLE DATABASE KURULUMU

* [Github Sitesi Docker imges projesini indirin.](https://github.com/oracle/docker-images.git) 

* Projede Oracle19c standart edition 2 kullanılmıştır. Bu linki kullanarak indirebilirsiniz. [Oracle DB 19c](https://www.oracle.com/tr/database/technologies/oracle19c-linux-downloads.html#license-lightbox")

* Windows'dan Linux'a dosya aktarımı için WinSCP programını indirebilirsiniz. [WinSCP link](https://winscp.net/eng/download.php)
* Oracle'dan indirdiğimiz yükleme dosyasını **\docker-images\OracleDatabase\SingleInstance\dockerfiles\19.3.0** konumuna yerleştirin.
* WinSCP ile docker-images klasörünü VM'e aktaralım.

![unknown.png (1607×773)](https://cdn.discordapp.com/attachments/861539670293479430/874269873772044298/unknown.png)



* Linux konsolunda buildContainerImage.sh dosyasının bulunduğu dizine gelip aşağıdaki kodu uygulayın.

  **Örnek dosya yolu: **/home/argela/docker-images/OracleDatabase/SingleInstance/dockerfiles

```bash
./buildContainerImage.sh -v 19.3.0 -s
```

![unknown.png (786×420)](https://cdn.discordapp.com/attachments/861539670293479430/874271430160826368/unknown.png)

* Oracle DB için volume oluşturuyoruz.

```bash
docker volume create oracle-db
```

* Oracle Container'ı oluşturuyoruz.

```bash
docker run -d --name oracledb -p 1521:1521 -p 5500:5500 -e ORACLE_SID=test -e ORACLE_PWD=1234 -v oracle-db:/opt/oracle/oradata oracle/database:19.3.0-se2
```

* Oracle DB'yi oluşturduğumuz Network'e ekliyoruz.

```bash
docker network connect --ip 172.20.128.3 multi-host-network oracledb
```

##### ZOOKEEPER KURULUMU

```bash
docker run -d --name zookeeper -p 2182:2181 zookeeper
```



##### KAFKA KURULUMU

```bash
docker run -h myhbase --name kafka -p 9092:9092 -e KAFKA_ZOOKEEPER_CONNECT={ip adresin}:2182 -e KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://{ip adresin}:9092 -e KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR=1 confluentinc/cp-kafka
```



##### HBASE KURULUMU

```bash
docker run -d -h myhbase --ip 172.20.128.6 --network multi-host-network --name hbase -p 2181:2181 -p 8080:8080 -p 8085:8085 -p 9090:9090 -p 9095:9095 -p 16000:16000 -p 16010:16010 -p 16020:16020 -p 16030:16030  -p 16201:16201 -p 16301:16301 harisekhon/hbase
```

* Hbase haberleşme esnasında hostname kullanmaktadır. Bu sebepten VM üzerinde hostname'i tanımlamamız gerekmektedir.

```bash
sudo nano /etc/hosts
```

```tex
192.168.8.200 myvm    //ip'ler VM'in ip'sini temsil etmektedir.
192.168.8.200 myhbase 
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
```

![unknown.png (804×715)](https://cdn.discordapp.com/attachments/861539670293479430/874273389097934848/unknown.png)