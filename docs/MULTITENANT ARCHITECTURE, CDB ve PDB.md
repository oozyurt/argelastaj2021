## Multitenant Architecture

* Öncelikle multitenant architecture yani çoklu-kiracılı mimarinin tam olarak ne olduğunu öğrenmeliyiz. Bu mimariye göre tasarlanan yazılımlar tek bir yazılım kodu kullanarak bir çok firma veya kiracıya hizmet vermeyi amaçlar. Burada esas zor olan, bu yazılımların, hizmet ettiği her bir organizasyon için kendisini sanal olarak parçalayabilmesi ve bu organizasyonlara özel uyarlamalar yapılabilmesine olanak tanımasıdır. Oracle'da bu mimariyi CDB ile ortaya koymaya çalışmaktadır.

![img](https://cdn.discordapp.com/attachments/861539670293479433/875725305338273792/11gJoOjs-meGUk8btzxtsSw.png)

## CDB(Container DB) ve PDB (Pluggable DB)

*  Oracle 12c versiyonu ile birlikte 1 tane ana veritabanı dediğimiz Container Database (CDB) üzerinde birden fazla Pluggable Database (PDB) dediğimiz alt veritabanları bulunmaya başlamıştır. Oracle Corporation, yazılımın çalışması sırasında kaynakları daha effektif kullanmak için 12c itibariyle CDB ile PDB arasında Background process, Shared memory, Oracle Metadata sı gibi bir çok özellikleri ortak kullanmakta ayrıca Control file, Redolog file ve Undo tablespace  gibi alanlarda her iki database arasında ortak olarak kullanılmaktadır.

* CDB'nin yapısına baktığımızda 3 temel component'ın olduğunu söyleyebiliriz.

  * Root:  `CDB$ROOT`olarak isimlendirilmiştir. CDB'nin yalnızca bir adet root'u bulunmaktadır. Metadata ile ortak kullanıcıları depolar. Ortak bir kullanıcı, rootta ve mevcut ve gelecekteki her PDB'DE aynı kimliğe sahip bir kullanıcı demektir.

  * Seed:  `PDB$SEED` olarak isimlendirilmiştir. Yeni PDB'ler oluşturmak için kullanabileceğiniz bir şablondur. Seed'de herhangi bir objeyi değiştiremez ve yeni bir obje ekleyemezsiniz. Bir CDB'nin yalnızca bir seed'i vardır.
  * PDB: PDB, kullanıcılara ve uygulamalara non-CDB  bir PDB gibi görünür. Oracle'ın 12c versiyonundan önce CDB özelliği olmadığından dolayı PDB'ler non-CDB olarak adlandırılıyordu. PDB'nin işlevine örnek vericek olursak belirli bir uygulamayı desteklemek için gereken verileri ve kodu içerebilir.

* Her bir component aslında bir containerdır. Hepsinin özel bir ID'si bulunmaktadır.

![img](https://cdn.discordapp.com/attachments/861539670293479433/875733241758375946/GUID-3D19575A-2C78-4FD5-B2D2-435F17A4F0D6-default.png)

## Önemli Kodlar

Bağlanılan ortam CDB mi PDB mi diye aşağıdaki gibi sorgulanır.

* Öncelikle ubuntu konsolumuzdan docker'daki Oracle containerımıza bağlanıyoruz.

  ```bash
  docker exec -it oracle /bin/bash
  ```

* Sonrasında gelen komut satırına yazılır.

  ```bash
  sqlplus sys@test as sysdba  
  ```

* SQL plus docker run komutunda oluşturmuş olduğumuz şifreyi isteyecektir. Şifremizi girip SQL komutlarını uygulayabiliriz.

* **SHOW CON_NAME** diyerek bulunduğumuz containeri öğreniyoruz.

* **SHOW CON_ID** diyerek containerın ID'sini öğreniyoruz.

  ![img](https://cdn.discordapp.com/attachments/861539670293479433/875736378346995752/unknown.png)

* ```bash
  COLUMN name FORMAT A30
  SELECT name, pdb FROM v$services ORDER BY name;
  ```

* Kodunu kullanarak PDB'lerimizin bulunduğu servisler ve isimler öğrenilir.

![img](https://cdn.discordapp.com/attachments/861539670293479433/875736878031179816/unknown.png)

* Databaseler arası geçiş yapmak için alttaki kodu kullanıyoruz.

  ```bash
  ALTER SESSION SET container = "CONTAINER İSMİ";
  ```

  ![img](https://cdn.discordapp.com/attachments/861539670293479433/875737447273750528/unknown.png)

* CDB'ye geçmek için bu kodu kullanıyoruz.

```bash
ALTER SESSION SET container = cdb$root;
```

![img](https://cdn.discordapp.com/attachments/861539670293479433/875738066231386112/unknown.png)

## REFERANSLAR

* https://docs.oracle.com/database/121/ADMIN/cdb_intro.htm#ADMIN14145
* https://ittutorial.org/oracle-12c-container-pluggable-database-operasyonlari/
* https://halilkabaca.wordpress.com/2011/03/10/coklu-kiracili-mimari-multitanency-nedir/