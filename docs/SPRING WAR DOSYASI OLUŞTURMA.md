## WAR DOSYASI OLUŞTURMA

* Spring Boot uygulaması yürütülebilir bir JAR dosyasında paketlenebilir. Bu dosya ile uygulamayı herhangi bir Web Sunucusuna dağıtmadan doğrudan çalıştırabilirsiniz. Yürütülebilir JAR dosyası tüm Web sunucularıyla uyumlu olamaz, bu nedenle onu bir Web sunucusuna dağıtmak istiyorsanız Spring Boot uygulamasını bir WAR dosyasına paketlemeniz gerekir.



![img](https://cdn.discordapp.com/attachments/861539670293479430/870300036213383188/sp.PNG)



* Spring projemizi oluştururken, spring.io sitesinde paket yöntemini WAR  java versiyonunu 8 olarak seçmeniz gerekmektedir. Bağımlılıkları seçerken buradan ekleyebilir ya da pom.xml dosyası üzerinden değişiklikleri gerçekleştirebiliriz.

* Projeyi oluşturduktan sonra main paketinin altında webapp isimli klasör oluşturun. webapp klasörünün altına da WEB-INF klasörü oluşturun.

![img](https://cdn.discordapp.com/attachments/861539670293479430/870301892893356132/Ekran_Alnts.PNG)

* Sonrasında WEB-INF klasörünün altına dispatcherServlet-servlet.xml ve weblogic.xml dosyalarını oluşturun. (Weblogic Server için gerekli olan xml dosyaları)



##### dispatcherServlet-servlet.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
    http://www.springframework.org/schema/beans/spring-beans.xsd">

</beans>
```



##### weblogic.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<wls:weblogic-web-app
        xmlns:wls="http://xmlns.oracle.com/weblogic/weblogic-web-app"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://xmlns.oracle.com/weblogic/weblogic-web-app
        http://xmlns.oracle.com/weblogic/weblogic-web-app/1.4/weblogic-web-app.xsd">

    <wls:context-root>/myweb</wls:context-root>
    <wls:container-descriptor>
        <wls:prefer-application-packages>
            <wls:package-name>org.slf4j.*</wls:package-name>
            <wls:package-name>org.springframework.*</wls:package-name>
        </wls:prefer-application-packages>
    </wls:container-descriptor>

</wls:weblogic-web-app>
```



* Weblogic Server için ServletInitializer.java dosyasına ihtiyaç duyulmaktadır. Springboot otomatik olarak gelmekte eğer yoksa manuel olarak eklenmelidir.

```java
public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SpringBootWebLogicApplication.class);
    }

}
```



#### WAR DOSYASININ OLUŞTURULMASI

* **Eclipse için**

![img](https://cdn.discordapp.com/attachments/861539670293479430/870303956738375710/Ekran_Goruntusu_747.png)



* **IntelliJ için**

![img](https://cdn.discordapp.com/attachments/861539670293479430/870303959368216616/Ekran_Goruntusu_748_LI.jpg)



* İşlemler tamamlandıktan sonra WAR dosyasını target klasörü içersinde bulabilirsiniz. 

![img](https://cdn.discordapp.com/attachments/861539670293479430/870305103968616518/Inkedaa_LI.jpg)