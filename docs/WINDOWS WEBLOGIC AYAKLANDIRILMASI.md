



## WINDOWS WEBLOGIC AYAKLANDIRILMASI

#### DOCKER ORACLE WEBLOGIC KURULUMU

##### 1.ADIM

1. Dilediğiniz bir dizine weblogic klasörü oluşturun.
2. Oluşturduğunuz klasörün içine domain.properties dosyasını oluşturun.
3. domain.properties dosyasını metin editörü yardımı ile resimdeki gibi düzenleyin.

![img](https://cdn.discordapp.com/attachments/861539670293479430/869185087076454420/prop.PNG)

4. [oracle container](https://container-registry.oracle.com/) sitesinde hesabınız yoksa yeni hesap oluşturun.

   ![img](https://cdn.discordapp.com/attachments/861539670293479430/869189023921471598/unknown.png)

![img](https://cdn.discordapp.com/attachments/861539670293479430/869189631579652116/unknown.png)

![img](https://cdn.discordapp.com/attachments/861539670293479430/869189244839669810/unknown.png)

5. Kullanım koşullarını kabul ettikten sonra konsol üzerinde docker login komutuyla oturum açın.

```bash
docker login container-registry.oracle.com
```



![img](https://cdn.discordapp.com/attachments/861539670293479430/869186685517299752/log.PNG)

6. Oturum başarılı mesajı alındıktan sonra aşağıdaki komutu uygulayarak docker imajını bilgisayarınıza çekin.

* Weblogic 14c versiyonu için uygulanacak kod:

```powershell
docker pull container-registry.oracle.com/middleware/weblogic:14.1.1.0-dev-8
```

* Weblogic 12c versiyonu için uygulanacak kod:

  ```powershell
  docker pull container-registry.oracle.com/middleware/weblogic:12.2.1.4-dev-ol8
  ```

  

![img](https://cdn.discordapp.com/attachments/861539670293479430/869190659767144488/pull.PNG)

##### 2.ADIM

#### DOCKER NETWORK KURULUMU

* Docker da containerların biribirleriyle haberleşebilmesi için network yapısını oluşturuyoruz. 

```bash
docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 multi-host-network
```

* domain.properties dosyasının bulunduğu dizinde WSL Linux Terminalinizi açın.

![img](https://cdn.discordapp.com/attachments/861539670293479430/869191884218720316/Ekran_Alnts.PNG)

* Weblogic 14c için aşağıdaki docker run komutunu uygulayın.

  ```bash
  docker run -d --name weblogic14 --ip 172.20.128.2 --network multi-host-network -p 7001:7001 -p 9002:9002 -p 7003:7003 -p 7005:7005 -v $PWD:/u01/oracle/properties -v $PWD/user_pts:/u01/oracle/user_projects container-registry.oracle.com/middleware/weblogic:14.1.1.0-dev-8
  ```

* Weblogic 12c için aşağıdaki docker run komutunu uygulayın.

  ```bash
  docker run -d --name weblogic12 --ip 172.20.128.2 --network multi-host-network -p 7001:7001 -p 9002:9002 -p 7003:7003 -p 7005:7005 -v $PWD:/u01/oracle/properties -v $PWD/user_pts:/u01/oracle/user_projects container-registry.oracle.com/middleware/weblogic:12.2.1.4-dev-ol8
  ```

  

![img](https://cdn.discordapp.com/attachments/861539670293479430/869192519278280754/web.PNG)



##### 3.ADIM:WEBLOGIC SERVER ADMIN KONSOLA BAĞLANTI

* Web tarayıcı açın. Link kısmına [https://localhost:9002/console](https://localhost:9002/console) yazın.

* Gelen güvenlik uyarısını görmezden gelerek devam ediniz.

  ![img](https://cdn.discordapp.com/attachments/861539670293479430/869194613230034964/unknown.png)



* domain.properties dosyasında oluşturmuş olduğunuz kullanıcı adı ve şifreyle giriş yapabilirsiniz. 

![img](https://cdn.discordapp.com/attachments/861539670293479430/869194907154272286/unknown.png)



##### 4.ADIM: JAVA UYGULAMASININ WEBLOGIC ÜZERİNDE AYAKLANDIRILMASI(İSTEĞE BAĞLI)

* WAR olarak paketlediğimiz Maven projesini weblogic klasörüne kopyalayın. 

  ![img](https://cdn.discordapp.com/attachments/861539670293479430/869196226246086656/unknown.png)

* **Referans:** [WAR Dosyası oluşturmak-TR](https://bitbucket.org/oozyurt/argelastaj2021/src/master/docs/SPRING%20WAR%20DOSYASI%20OLUŞTURMA.md)

* **Referans:** [WAR Dosyası oluşturmak-EN](https://o7planning.org/11901/deploy-spring-boot-application-on-oracle-weblogic-server)


* Weblogic server'da Deployments sekmesine gelin.

  

![img](https://cdn.discordapp.com/attachments/861539670293479430/869197301317533756/unknown.png)

* Açılan sayfada install butonuna tıklayıp projeyi yüklemeye başlayın.



![img](https://cdn.discordapp.com/attachments/861539670293479430/869197741132247080/unknown.png)



* Path alanını /u01/oracle/properties olarak düzenleyin. Aşağıdaki görselde olduğu gibi WAR projeniz gözükecektir.

![img](https://cdn.discordapp.com/attachments/861539670293479430/869198253680365618/unknown.png)

* WAR dosyasını seçtikten sonra uygulamanın bağımsız çalışabilmesi için ilk seçeneğe tıklayarak ilerleyin. 

  

![img](https://cdn.discordapp.com/attachments/861539670293479430/869199194018160730/app.PNG)



* Yukarıdaki adımdan sonra gelecek iki adımda değişiklik yapmadan devam edin. Kurulum tamamlandıktan sonra gelecek ekranda Testing tabına gelerek uygulamanın yayın yaptığı ismi görebilirsiniz.



![img](https://cdn.discordapp.com/attachments/861539670293479430/869200710342959114/default.PNG)

* Web tarayıcıda localhost:7001/weblogicdemo linkini girerek uygulamaya erişebilirsiniz.



![img](https://cdn.discordapp.com/attachments/861539670293479430/869204576107122688/wh.PNG)

![img](https://cdn.discordapp.com/attachments/861539670293479430/869204578070069258/suc.PNG)

##### ADMIN SERVER CONTAINER'I ÜZERİNDE NODE MANAGER VE MANAGED SERVER AYAKLANDIRILMASI

* Docker containerları oluşturulurken eşsiz bir isim belirler. Bu klasör yolunu bulabilmek adına aşağıdaki kodu uygulayarak **UpperDir** yolunu takip ederek klasöre ulaşabiliriz. 

```bash
docker inspect weblogic12
```



![unknown.png (1216×244)](https://cdn.discordapp.com/attachments/861539670293479430/874626223177891890/unknown.png)



* Sonrasında bu yola gidebilmek için root kullanıcı yetkisi almalıyız. 

```bash
sudo su  
cd /var/lib/docker/overlay2/2d4846002a74efbbe02603d615c6b138eb2820061b7605cb424d0a79ab71b7a4/diff/u01/oracle/user_projects/domains/base_domain/nodemanager
```



* Belirtilen dizinde nodemanager klasörüne ulaştıktan sonra **nodemanager.properties** dosyasını bir text editörü yardımıyla düzenleyerek **SecureListener** parametresini **true** değerinden **false** değerine getirelim.

```bash
nano nodemanager.properties
```

* nano yoksa **sudo yum install nano** olarak yükleyebilirsiniz.

![unknown.png (1027×143)](https://cdn.discordapp.com/attachments/861539670293479430/874627864237047828/unknown.png)

![unknown.png (569×191)](https://cdn.discordapp.com/attachments/861539670293479430/874627935649267762/unknown.png)



* Node manager'ı ayaklandırmak için aşağıdaki kodu kullanalım.

```bash
docker exec -d weblogic12 ./user_projects/domains/base_domain/bin/startNodeManager.sh
```



##### MACHINE VE MANAGED SERVER OLUŞTURMA

###### MACHINE OLUŞTURMA

* Weblogic de Environment altında **Machines** alanına gidelim. **"New"** tuşuna basarak yeni bir makine oluşturalım. İsim alanına istediğimiz bir ismi girelim.

![unknown.png (370×356)](https://cdn.discordapp.com/attachments/861539670293479430/874629810415108116/unknown.png)

![unknown.png (639×523)](https://cdn.discordapp.com/attachments/861539670293479430/874629959694565436/unknown.png)



* Type alanını **Plain** seçelim. Finish tuşuna basın.

![unknown.png (684×643)](https://cdn.discordapp.com/attachments/861539670293479430/874630021950627900/unknown.png)



###### MANAGED SERVER OLUŞTURMA

* Weblogic de Environment altında **Servers** alanına gidelim. **"New"** tuşuna basarak yeni bir server oluşturalım.
* Docker da açtığımız portlar arasında 7003 olduğu için Server Listen Port değerine 7003 verdik.

![unknown.png (938×592)](https://cdn.discordapp.com/attachments/861539670293479430/874631462484328528/unknown.png)

* Oluşturduğumuz server da, machine seçimini yapalım.

![unknown.png (1197×670)](https://cdn.discordapp.com/attachments/861539670293479430/874632024730779668/unknown.png)



* Machine seçimi yapıldıktan sonra save butonuna basalım. Sonrasında Advanced sekmesini açarak **Local Administration Port Override** parametresini değiştirelim.

![unknown.png (1114×980)](https://cdn.discordapp.com/attachments/861539670293479430/874632063293194251/unknown.png)



* Node manager'ın açık olduğundan emin olun. Node manager'ın ulaşılabilir olduğunu oluşturduğumuz Machine altında Monitoring sekmesinden görebiliriz.

![unknown.png (1822×422)](https://cdn.discordapp.com/attachments/861539670293479430/874633530553028628/unknown.png)



* Servers kısmında Control sekmesine giderek oluşturduğumuz Server'ı ayaklandırabiliriz.

![unknown.png (1579×626)](https://cdn.discordapp.com/attachments/861539670293479430/874633597334720573/unknown.png)



#### REFERANSLAR:

* [container-oracle](container-registry.oracle.com)
* [oracle-weblogic-server-docker hub](https://hub.docker.com/_/oracle-weblogic-server-12c)
* [Weblogic WAR dosyası oluşturma](https://o7planning.org/11901/deploy-spring-boot-application-on-oracle-weblogic-server)
* [Weblogic tanıtımı](https://www.youtube.com/watch?v=h7EId_N6VyQ)
* [WAR dosyası](https://drive.google.com/file/d/1_drcMxwy05ZbBO10fZR7glr-rjkrmqEF/view?usp=sharing)