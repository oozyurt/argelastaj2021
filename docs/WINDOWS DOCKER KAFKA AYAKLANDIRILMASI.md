## WINDOWS DOCKER KAFKA VE ZOOKEEPER AYAKLANDIRILMASI 

### ÖN AYAR:  ###

* Kafka yönetimi sağlayabilmesi için Zookeeper'a ihtiyaç duymaktadır. Projemiz de ayrı bir zookeeper ayaklandırılması gerçekleştirilecektir.
* Zookeeper docker image' ında Hbase' in 2181 portunu kullanmasından dolayı Zookeeper da 2182 portunu kullanmaktayız.



#### DOCKER ZOOKEEPER KOMUTUNUN UYGULANMASI

* Zookeeper kurulumu için aşağıdaki kod satırını yazmanız yeterlidir.

```bash
docker run -d --name zookeeper -p 2182:2181 zookeeper
```



#### DOCKER KAFKA KOMUTUNUN UYGULANMASI ####

```powershell
docker run -h myhbase --name kafka -p 9092:9092 -e KAFKA_ZOOKEEPER_CONNECT={ip adresin}:2182 -e KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://{ip adresin}:9092 -e KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR=1 confluentinc/cp-kafka
```



![img](https://cdn.discordapp.com/attachments/861539670293479430/869591181443600465/kf.PNG)



* **docker run:** Docker'da imageların çalışmasını sağlar.

* **-d:** Docker containerın arka planda çalışmasını sağlar.

* **-h:** Docker'da hostname parametresini belirler.

* **-p:** Docker ortamındaki portları yerel bilgisayarımızda kullanmamızı sağlar. 

  Örnek kullanımı: **(-p {yerel port}:{docker port})**

* Kafka, 9092 portunu kullanmaktadır.

* **-name:** Docker'da bu tag, containerın ismini manuel olarak vermemizi sağlar.

  Docker'da kullandığımız image:  **confluentinc/cp-kafka:{versiyon}**

* ***Not:*** Image'ın yanına versiyon belirtmediğimiz zaman Docker otomatik olarak en son versiyonu"latest" indirecektir.

* **-e:** Ortam değişkenleri parametlerini belirlemek için kullanılır.

* **{ip adresin}:** Bu alan yerel bilgisayarın ip adresi ile değiştirilmelidir. 

  Örnek: KAFKA_ZOOKEEPER_CONNECT=192.168.0.11:2182



