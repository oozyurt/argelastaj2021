## WINDOWS RABBİTMQ AYAKLANDIRILMASI ##

#### DOCKER NETWORK KURULUMU

* Docker da containerların biribirleriyle haberleşebilmesi için network yapısını oluşturuyoruz. 

```bash
docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 multi-host-network
```

#### RABBITMQ KURULUM ####

```powershell
docker run -d  --ip 172.20.128.7 --network multi-host-network --hostname myhbase -p 5672:5672 -p 15672:15672 --name rabbitmq rabbitmq:3-management
```



* **docker run:** Docker'da imageların çalışmasını sağlar.

* **-d:** Docker containerın arka planda çalışmasını sağlar.

* **--hostname:** Docker'da hostname parametresini belirler.

* **-p:** Docker ortamındaki portları yerel bilgisayarımızda kullanmamızı sağlar. 

  Örnek kullanımı: **(-p {yerel port}:{docker port})**

* Rabbitmq, 5672 portunu kullanmaktadır.

* Rabbitmq'nun management arayüzü 15672 portunu kullanmaktadır.

* **-name:** Docker'da bu tag, containerın ismini manuel olarak vermemizi sağlar.

  Docker'da kullandığımız image:  **rabbitmq:3-management**

* ***Not:*** Image'ın yanına versiyon belirtmediğimiz zaman Docker otomatik olarak en son versiyonu"latest" indirecektir.




##### RABBIT MANAGEMENT CONSOLE
* Yönetim Konsoluna girmek için http://localhost:15672 linkini kullanın.
* RabbitMQ standart olarak kullanıcı adı ve şifresi 'guest' olarak tanımlanmıştır.
***Not:*** Windows'da bazı portlar önceden kullanılıyor olabilir, Docker imagelarımızın stabil çalışması için açık portlarımızı kontrol etmemiz önemlidir. Bunun için terminalde **netstat** komutu uygulayarak açık portlarımızı kontrol edebiliriz.



![img](https://cdn.discordapp.com/attachments/861539670293479430/864476367458664509/unknown.png)



