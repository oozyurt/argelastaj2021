## WINDOWS DOCKER HBASE AYAKLANDIRILMASI 

### ÖN AYAR: ###

* Windows/System32/drivers/etc konumunda bulunan hosts dosyasını yönetici haklarıyla açın. 

  ![img](https://cdn.discordapp.com/attachments/861539670293479430/863033208588075049/unknown.png)  

Dosyayı açtıktan sonra ip adresinizle beraber örnek olarak: myhbase hostname'ni ekleyerek kaydedin ve dosyayı kapatın.

![img](https://cdn.discordapp.com/attachments/861539670293479430/863034368833617930/unknown.png)

* Host düzenlemesini WSL ubuntu üzerinde gerçekleştiriyoruz. 

```bash
sudo nano /etc/hosts
```

* Bu kodu yazdıktan sonra yönetici haklarıyla açtığımız dosyaya yukarıda belirtmiş olduğumuz windows host bilgilerini giriyoruz. Kaydetmek için **Ctrl+O** ardından **Enter** , çıkış için **Ctrl+X**  

![img](https://cdn.discordapp.com/attachments/861539670293479430/863031981478641674/Ekran_Goruntusu_707.png)



#### METOT 1: KONSOL KOMUTU

#### DOCKER NETWORK KURULUMU

* Docker da containerların biribirleriyle haberleşebilmesi için network yapısını oluşturuyoruz. 

```bash
docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 multi-host-network
```



* Bu yöntemde hbase uygulamasını Docker üzerinde konsol komutu yazarak ayaklandırıyoruz.

  ```powershell
  docker run -d -h myhbase --ip 172.20.128.6 --network multi-host-network --name hbase -p 2181:2181 -p 8080:8080 -p 8085:8085 -p 9090:9090 -p 9095:9095 -p 16000:16000 -p 16010:16010 -p 16020:16020 -p 16030:16030  -p 16201:16201 -p 16301:16301 harisekhon/hbase
  ```

  

* **docker run:** Docker'da imageların çalışmasını sağlar.

* **-d:** Docker containerın arka planda çalışmasını sağlar.

* **-h:** Docker'da hostname parametresini belirler.

* **-p:** Docker ortamındaki portları yerel bilgisayarımızda kullanmamızı sağlar. 

  Örnek kullanımı: **(-p {yerel port}:{docker port})**

* **-name:** Docker'da bu tag, containerın ismini manuel olarak vermemizi sağlar.

   Docker'da kullandığımız image:  **harisekhon/hbase:{versiyon}**

***Not:*** Image'ın yanına versiyon belirtmediğimiz zaman Docker otomatik olarak en son versiyonu"latest" indirecektir.



#### METOT 2: DOCKER COMPOSE DOSYASININ KULLANILMASI  ####

![img](https://cdn.discordapp.com/attachments/861539670293479430/863040420531404810/Ekran_Alnts.PNG) 

* Docker-compose.yml dosyasını kullanabilmemiz için bulunduğu dizinden terminal komutu yazmamız gerekmektedir. 

  ```powershell
  docker-compose -f docker-hbase.yml up -d
  ```

  

***Not:*** Windows'da bazı portlar önceden kullanılıyor olabilir, Docker imagelarımızın stabil çalışması için açık portlarımızı kontrol etmemiz önemlidir. Bunun için terminalde **netstat** komutu uygulayarak açık portlarımızı kontrol edebiliriz.