## OpenShift

##### Nedir?

  Openshift, üzerinde web uygulamaları barındırmamızı sağlayan, node.js ,python, java, ruby, go, perl, php gibi dilleri destekleyen ve bunlara ait bir çok popüler uygulamayı tek tıklama ile kullanabilmeyi sağlayan bir PaaS (Platform as a Services) çözümüdür.
##### Temeli


  ![img](https://www.datasciencearth.com/wp-content/uploads/2021/05/OpenShift_02.png)

  Docker container'larının üzerinde oluştulurmuş Kubernetes'in daha pratik yönetilebilmesi için geliştirilmiş bir araçtır. Resimde de görüldüğü üzere en altta Docker Engine, onun üstüne kurulmuş Kubernetes onun üstünde ise OpenShift bulunmakta. Bu sebeple OpenShift'i anlamak için Docker ve Kubernetes'i de anlamamız gerekir.

##### Neler Sağlar?

* **SCM (Source Code Management)**: Github, BitBucket gibi araçların yanında lokal olarak kullanılabilen kaynak yönetimi araçlarından faydalanır. Şu anda yalnızca Git çözümlerini desteklemektedir.

* **Pipeline**: CI/CD bazlı çözüm sağlayan araçtır. Development ve deployment süreçlerini soyutlayarak tüm platformlarda çalışabilir hale getirmeyi amaçlar.

* **OCR (Openshift Container Registry)**: Docker konusundan da aşina olduğumuz bir konu. Image oluşturup saklamak, container oluştururken ihtiyacımız olan image’leri indirmek, image’leri yönetmek gibi bir dizi işlemi üzerinde gerçekleştiririz.

* **SDN (Software Defined Network)**: Docker ve Kubernetes’ten aşina olacağımız bir kavram. Container’ların birbirleri ile iletişim kurabilmesini olanaklı hale getirir. Overlay network’ten anımsayacaksınız.

* **API**: Yönetim işlemleri için Openshift üzerinde API’lar bulunur. Bu hem kaynakları hem de işlemleri yönetmek için kullanılan, REST temelli araçlardır.

* **Governance**: Kişi veya takımların uygulamalara erişimini yönetmek için kullanılır. Bu şekilde yetkisiz erişimlerin de önüne geçilmiş olur.



##### Avantajları



  * **Kullanıcı Arayüzü**

    OpenShift, hem administration hem de development olarak neredeyse bir çok Kubernetes işlemini bir web arayüzü üzerinden oluşturup geliştirmenize imkan sağlayan zengin bir arabirim sunar.

  * **Güvenlik**

    OpenShift, varsayılan olarak, her bir geliştiricinin hesaptan ödün verme sorununu önlemek için yalnızca ihtiyaç duyduğu özellikler için izin almasını sağlamaya yardımcı olan rol tabanlı erişim denetimi (RBAC) sunar. Openshift ile bir proje oluşturduğunuzda, IAM ve OAuth gibi diğer güvenlik kurallarının tümü varsayılan olarak oluşturulur. Yalnızca gerektiği gibi kullanıcı izinleri eklemeniz gerekir. Bu, uygulama ortamınızın kurulumunu kolaylaştırır ve size zaman kazandırır. Kubernetes tarafında, artık Kubernetes RBAC sunmasına rağmen, IAM ve OAuth gibi diğer güvenlik protokolleri de dahil olmak üzere her şeyi kendiniz ayarlamanız gerekir.

  * **Node Konfigurasyonu**

    Sanallaştırılmış olsun veya olmasın, sıkıntısız bir şekilde çok sayıda sanal makine kurabilirsiniz. Bir uygulama ek bir sanal makine gerektirdiğinde, Kubernetes'in süreci yönetmesi biraz karmaşık ve zaman alıcıdır, yeni VM'ler oluşturup bunları kümeye getirmeden önce kendi kendine kayıt veya farklı bulut otomasyonu kurmak için komut dosyaları geliştirmenizi gerektirir. Bu süreç biraz yorucu ve zaman alıcı olabilir. Red Hat'in OpenShift'i bunu çok daha kolay bir şekilde ele alıyor. Yeni sanal makineleri kümeye kolayca getirmek için sağlanan **Ansible** çalışma kitaplarını ve yükleyicileri kullanabilirsiniz. Ayrıca, iş yükünü artırmak için kümeye yeni sanal makinelerin otomatik olarak eklenmesini sağlayan otomatik ölçeklendirme sağlar.

  * **Workflow Otomasyonu**

    OpenShift, Python, Java, PHP, Go, Node.js ve Ruby dahil olmak üzere birçok programlama dilini destekler. Uygulama geliştirmenizi hızlı bir şekilde başlatmak için bu dillerin ve çerçevelerin çoğunda önceden oluşturulmuş uygulama şablonları sağlar. Eskiden Kubernetes şablonları, OpenShift şablonlarından çok daha iyiydi. Yeni versiyonuyla birlikte bir uygulamayı dağıtırken kullanabileceğiniz iki şablon sağladı: Kubernetes Helm Charts ve OpenShift şablonu. Bu, geliştiriciye daha güzel bir şablon seçenekleri yelpazesi sunar. OpenShift ayrıca, Openshift'i DockerHub veya Red Hat ile otomatik olarak eşleştiren ve görüntü akışlarını kullanarak kapsayıcı görüntülerini kolayca yönetmenize olanak tanıyan kaynaktan görüntüye özelliğini kullanan yerleşik bir görüntü kaydına sahiptir. OpenShift ile özel görüntü kayıt defterinizi de kullanabilirsiniz. Öte yandan Kubernetes, bu tür bir entegre görüntü yönetimi sunmaz ve ayrıca iş akışlarının çoğu için manuel yapılandırma gerektirir.

  * **Platformlar Arası Uyumluluk**

    Kubernetes her ne kadar bulut uyumlu olsa da platformlar arası özel eklentiler, yapılandırmaları olan bir sistem. Herhangi bir projeyi bir platformdan diğerine aktarmak için öncelikle o platform üzerindeki Kubernates çözümü hakkında bilgi sahibi olmak gerekir. OpenShift ise herhangi bir bulut sağlayacısında aynı web arayüzüyle proje geliştirmeye olanak sağlar

![img](https://cdn.discordapp.com/attachments/861539670293479433/879695276883984404/im3.png)



##### Lisans Politikası

Kullanabileceğimiz dört sürüm bulunmaktadır.

  * OpenShift OKD: Açık kaynak kodlu sürüm. Ücretsiz kullanım sağlar
  * OpenShift Online: Hosting üzerinde online sürüm.
  * OpenShift Dedicated: Google veya AWS gibi cloud çözümlerinde kullanabilir sürüm
  * OpenShift Enterprise: PaaS olarak lisanslı kullanabilir sürüm. Ücretli kullanım ve destek sağlar.



#### Kurulum

 Kişisel bilgisayarımıza kurmak için Minishift çözümünü kullanmamız gerekiyor.

  ##### Gereksinimler

 * VirtualBox kurulu olması gerekli.
 * Mac bilgisayarlarda hyperkit, Windows bilgisayarlarda ise Hyper-V desteğinin kurulu/açık olması gerekiyor.

    ##### hyperkit Kurulumu
    **Bilgisayarınızda Homebrew paketinin yüklü olduğuna emin olun**
    Komut satırına aşağıdaki komutu girmeniz yeterli.
    
  ```console
     brew install hyperkit
  ```
  **Mac Bilgisayarlar için:**
  ```console
 brew install minishift
  ```
  Kurulumdan sonra start vermek için ise vm-driver flag'ini kullanacağız.
  ```console
   minishift start --vm-driver=virtualbox
  ```
  Daha sonra sanal makinanızın IP adresinin 8443 portuna giderek console dalıyla web konsola ulaşabilirsiniz. Https kullanımına dikkat ediniz.
  _Örnek kullanım: https://192.168.99.100:8443/console_
  _Kullanıcı adı: developer
  Şifre: Herhangi bir şey yazabilirsiniz_
  ![img](https://miro.medium.com/max/700/1*TEK7BC8aUz-0YWFWcFFhzA.png)





##### Referanslar

* https://medium.com/devopsturkiye/openshift-nedir-188aa22934a3
* https://www.datasciencearth.com/openshift-bolum-1-openshift-nedir/
* https://thechief.io/c/editorial/7-advantages-openshift-over-kubernetes/

