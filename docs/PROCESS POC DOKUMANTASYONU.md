## PROCESS POC DOKUMANTASYONU ##



![Resim önizlemesi](https://cdn.discordapp.com/attachments/861539670293479430/864876454499188747/btm-sgw.png)



### ÖN GEREKSİNİMLER ###

* Proje Docker containerler üzerinde geliştirilmektedir. 
* Gerekli olan Docker containerları: 
  * Hbase
  * Kafka
  * MySQL
  * RabbitMQ

#### BTM PROJESİ ####

* Springboot 2.5.2 projesidir. Java JDK 1.8 ile yazılmıştır. Proje rest servisten aldığı veriyi Kafka aracılığıyla Hbase tablosuna iletmektedir. 

##### POM.XML #####

* Projemizde yazdığımız kodlar bu bağımlılıkları gerektirmektedir.

```xml
 <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web-services</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.kafka</groupId>
            <artifactId>spring-kafka</artifactId>
        </dependency>       
        <dependency>
            <groupId>org.apache.hbase</groupId>
            <artifactId>hbase-client</artifactId>
            <version>2.3.5</version>
        </dependency>
        <dependency>
            <groupId>org.apache.hbase</groupId>
            <artifactId>hbase-common</artifactId>
            <version>2.3.5</version>
        </dependency>
        <dependency>
            <groupId>org.apache.hbase</groupId>
            <artifactId>hbase</artifactId>
            <version>2.3.5</version>
            <type>pom</type>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-amqp</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.amqp</groupId>
            <artifactId>spring-rabbit-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
```



##### APPLICATION.PROPERTIES

* 3. parti uygulamalara erişim sağlamak için bu konfigurasyonları uygulamalıyız.

```properties
server.port=17700

spring.kafka.consumer.bootstrap-servers: localhost:9092
spring.kafka.consumer.group-id: group-id
spring.kafka.consumer.auto-offset-reset: earliest
spring.kafka.consumer.key-deserializer: org.apache.kafka.common.serialization.StringDeserializer
spring.kafka.consumer.value-deserializer: org.springframework.kafka.support.serializer.JsonDeserializer
spring.kafka.consumer.properties.spring.json.trusted.packages=*

spring.kafka.producer.bootstrap-servers: localhost:9092
spring.kafka.producer.key-serializer: org.apache.kafka.common.serialization.StringSerializer
spring.kafka.producer.value-serializer: org.springframework.kafka.support.serializer.JsonSerializer


#RabbitMQ Connection
spring.rabbitmq.host=localhost
spring.rabbitmq.port=5672
spring.rabbitmq.username=argela
spring.rabbitmq.password=1234

#Exchange Configuration
sr.rabbit.queue.name=argela-queue
sr.rabbit.routing.name= argela-routing
sr.rabbit.exchange.name=argela-exchange

#HBase Configuration
sr.hbase.zk.quorum=myhbase
sr.hbase.zk.node=/hbase
sr.hbase.zk.port=2181
sr.hbase.super.user=hbase

```



##### USER MODEL #####

* Projemizde kullandığımız veri modeli yapısı.

```java
public class User {    
    private String uuid;
    private String name;
    private String surname;
    private int age;
    private String gender;}
```



##### HBASE KONFİGURASYONU #####

* Hbase bağlantısı için gereken konfigurasyon kodu.

```java
@org.springframework.context.annotation.Configuration
public class Hbase {
    //value değerleri application.properties den gelmektedir.
    @Value("${sr.hbase.zk.quorum}") 
    private String ZK_QUORUM;  //zookeeper ın bulunduğu sunucu.
    @Value("${sr.hbase.zk.node}")  
    private String ZK_ZNODE;   //zookeeper ın sunucuda bulunduğu dizin.
    @Value("${sr.hbase.zk.port}")
    private String ZK_PORT;    //zookeeper hizmet portu.
    @Value("${sr.hbase.super.user}")
    private String SUPER_USER;

    //Hbase için gerekli olan konfigurasyon parametresini döndürmektedir.
    @Bean
    public Configuration getConfiguration() throws IOException {
        Configuration conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", ZK_QUORUM);
        conf.set("zookeeper.znode.parent", ZK_ZNODE);
        conf.set("hbase.zookeeper.property.clientPort", ZK_PORT);
        return conf;
    }

    public Configuration setUser(Configuration conf, String user) {
        UserGroupInformation.setConfiguration(conf);
        UserGroupInformation remoteUser = UserGroupInformation.createRemoteUser(user);
        UserGroupInformation.setLoginUser(remoteUser);
        return conf;
    }

    //Hbase bağlantısı için connection nesnesi döner.
    @Bean
    public Connection createConnection() throws IOException {
        Configuration configuration = setUser(getConfiguration(), SUPER_USER);
        Connection connection = ConnectionFactory.createConnection(configuration);
        return connection;
    }

}
```



* Bu konfigurasyon kodu Zookeeper aracılığıyla Hbase'e olan bağlantıyı gerçekleştirmektedir. 



##### HBASE HELPER #####

* Hbase'in CRUD operasyonları: 

```java

//Hbase'de tablo oluşturur.
public void createTable(Connection connection, String tableName, String... args) throws IOException {
        Admin admin = connection.getAdmin();
        TableDescriptorBuilder tdesBuilder = TableDescriptorBuilder.newBuilder(TableName.valueOf(tableName));
        ArrayList<ColumnFamilyDescriptor> cflist = new ArrayList<>();
        for (int i = 0; i < args.length; i++) {
            ColumnFamilyDescriptor cf = ColumnFamilyDescriptorBuilder.of(args[i]);
            cflist.add(cf);
        }
        tdesBuilder.setColumnFamilies(cflist);
        TableDescriptor table = tdesBuilder.build();
        if (admin.tableExists(TableName.valueOf(tableName))) {
            System.out.println("Table is exists.");
        } else {
            System.out.println("Creating table. ");
            admin.createTable(table);
        }
        admin.close();
    }

    //Hbase'de tabloyu siler.
    public void deleteTable(Connection connection, String tableName) throws IOException {
        Admin admin = connection.getAdmin();
        if (admin.tableExists(TableName.valueOf(tableName))) {
            System.out.print("Delete table. ");
            admin.disableTable(TableName.valueOf(tableName));
            admin.deleteTable(TableName.valueOf(tableName));
        }else {
            System.out.println("Table is not exists.");
        }
        admin.close();
    }


    //Hbase tablosuna veri ekler ve günceller.
    public void putRows(Connection connection, String tableName, String... args) throws IOException {
        Table table = connection.getTable(TableName.valueOf(tableName));
        ArrayList<Put> puts = new ArrayList<>();
        for (int i = 0; i < args.length; i++) {
            String[] row = args[i].split(":");
            Put put = new Put(Bytes.toBytes(row[0]));
            put.addColumn(Bytes.toBytes(row[1]), Bytes.toBytes(row[2]),
                    Bytes.toBytes(row[3]));
            puts.add(put);
        }
        table.put(puts);
        table.close();
    }

    //Hbase tablosundan veri çeker.
    public String getRows(Connection connection, String tableName, String rowkey) throws IOException {
        Table table = connection.getTable(TableName.valueOf(tableName));
        Get get = new Get(Bytes.toBytes(rowkey));
        Result result = table.get(get);
        String val="";
        for (Cell cell : result.rawCells()) {
            byte[] rk = CellUtil.cloneRow(cell);
            byte[] family = CellUtil.cloneFamily(cell);
            byte[] column = CellUtil.cloneQualifier(cell);
            byte[] value = CellUtil.cloneValue(cell);
            String kv = Bytes.toString(rk) + ":" + Bytes.toString(family) + ":" + Bytes.toString(column) + ":" + Bytes.toString(value);
            //System.out.println(kv);
            val+=kv+", ";
        }
        table.close();
        return val;
    }


```



##### REST CONTROLLER 

* Rest Controller web üzerinden gelen http isteklerini yönetmektedir. 

```java

@RestController
@RequestMapping("/api")
public class ApiController {

    private final Producer producer;
    @Autowired
    Hbase hbase;
    @Autowired
    public ApiController(Producer producer){
        this.producer=producer;
    }

    //Post metodu ile gönderilen user nesnesini producer'a iletir.
    @PostMapping("/save")
    public ResponseEntity<String> save(@RequestBody User user)throws IOException{
        user.setUuid(UUID.randomUUID().toString());
        this.producer.sendInfo(user,"test_topic");
        return new ResponseEntity<String>("Success", HttpStatus.OK);

    }
    
    //Get metodu ile parametre girilen row değerini Hbase tablosundan çekerek veriyi döndürür. 
    @GetMapping("/get/{row}")
    public ResponseEntity<String> get(@PathVariable String row) throws IOException {
        ArrayList<String> strings = new ArrayList<>();

        HBaseHelper helper =new HBaseHelper();

        return new ResponseEntity<String>(helper.getRows(hbase.createConnection()       ,"user",row),HttpStatus.OK);


    }

```



##### KAFKA TOPIC KONFİGURASYONU

* NewTopic ve TopicBuilder; Kafka broker üzerinde topic oluşturur. 

```java
@Configuration
public class TopicConfig {
    @Bean
    public NewTopic test_topic(){
        return TopicBuilder
                .name("test_topic")
                .build();
    }
}
```



##### KAFKA PRODUCER

* Rest arayüzünden gelen user nesnesi, KafkaTemplate yardımıyla topic alanına gönderilmektedir. 

```java
@Service
public class Producer {

    @Autowired
    private KafkaTemplate<String,Object> kafkaTemplate;

    public void sendInfo(User user, String topic){
        this.kafkaTemplate.send(topic,user);
    }

}
```



##### KAFKA CONSUMER

* İlgili topic alanına gelen veriyi Kafka Listener ile dinleyerek Hbase tablosuna yazdırır.

```java
@Service
public class Consumer  {

    @Autowired
    Hbase hbase;
    HBaseHelper myBase = new HBaseHelper();
    
    @KafkaListener(topics="test_topic", groupId="group_id")
    public void consumeMessage(User user) throws IOException {

        myBase.putRows(hbase.createConnection(),"user",new String[]{user.getUuid()+":personal_info:name:"+user.getName(),user.getUuid()+":personal_info:surname:"+user.getSurname(),user.getUuid()+":personal_info:age:"+user.getAge(),
                user.getUuid()+":personal_info:gender:"+user.getGender()});

        //System.out.println("Topic: test_topic "+ myBase.getRows(hbase.createConnection(), "user",user.uuid));
    }

}

```



##### RABBITMQ KONFİGURASYONU

* RabbitMQ bağlantısı için gereken konfigurasyon kodu.

```java
@Configuration
public class RabbitMQConfiguration {

    //Value ile application.properties de tanımlı konfigurasyon parametreleri kullanılır. 
    @Value("${sr.rabbit.queue.name}")
    private String queueName;  
    @Value("${sr.rabbit.routing.name}")
    private String routingName;
    @Value("${sr.rabbit.exchange.name}")
    private String exchangeName;

    //Kuyruğa gidip gelmek için kullanılan yapı.
    @Autowired
    private RabbitTemplate rabbitTemplate;
    
    //Kuyruk belirleyerek kuyruk nesnesini döner.
    @Bean
    public Queue queue(){
        return new Queue(queueName);

    }

    //RabbitMQ'de exchange yapısını döner.
    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange(exchangeName);
    }
    
    //Binding; exchange ve kuyruğu birbirine bağlar.
    @Bean
    public Binding binding(final Queue queue, final DirectExchange directExchange){
        return BindingBuilder.bind(queue).to(directExchange).with(routingName);
    }

}
```



***Not:*** Projemizde tek consumer olduğundan direct exchange mimarisi kullanılmıştır. 



##### RABBITMQ CONSUMER

* RabbitListener, "argela-queue" isimli kuyruğa gelen verileri dinlemektedir ve konsola yazdırmaktadır.

```java
@Service
public class UserListener {
    @RabbitListener(queues = "argela-queue")
    public void  handleMessage(String s){
        System.out.println("Message Received");

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        //Json'dan objeye dönüştürür.
        User  user = gson.fromJson(s,User.class);

        System.out.println("userlistener :"+user.toString());
    }

}
```



#### SGW PROJESİ

* Hbase tablosundan alınan veri MySQL  veritabanına gönderilir ve RabbitMQ kuyruk sunucusuna eklenir.



##### POM.XML

* Projemizde yazdığımız kodlar bu bağımlılıkları gerektirmektedir.

```xml
 <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-amqp</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-jpa</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <scope>runtime</scope>
            <optional>false</optional>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>     
          <groupId>com.google.code.gson</groupId>     
           <artifactId>gson</artifactId>     
        </dependency> 
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.amqp</groupId>
            <artifactId>spring-rabbit-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.hbase</groupId>
            <artifactId>hbase-client</artifactId>
            <version>2.3.5</version>
        </dependency>
        <dependency>
            <groupId>org.apache.hbase</groupId>
            <artifactId>hbase-common</artifactId>
            <version>2.3.5</version>
        </dependency>
        <dependency>
            <groupId>org.apache.hbase</groupId>
            <artifactId>hbase</artifactId>
            <version>2.3.5</version>
            <type>pom</type>
        </dependency>
    </dependencies>
```



##### APPLICATION.PROPERTIES

* 3. parti uygulamalara erişim sağlamak için bu konfigurasyonları uygulamalıyız.

```properties
#MySql Connection
spring.jpa.hibernate.show-sql=true
spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://localhost:3306/argela?useSSL=false&serverTimezone=UTC&useLegacyDatetimeCode=false&allowPublicKeyRetrieval=true
spring.datasource.username=root
spring.datasource.password=1234
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5InnoDBDialect
#RabbitMQ Connection
spring.rabbitmq.host=localhost
spring.rabbitmq.port=5672
spring.rabbitmq.username=argela
spring.rabbitmq.password=1234

#Exchange Configuration
sr.rabbit.queue.name=argela-queue
sr.rabbit.routing.name= argela-routing
sr.rabbit.exchange.name=argela-exchange

#HBase Configuration
sr.hbase.zk.quorum=myhbase
sr.hbase.zk.node=/hbase
sr.hbase.zk.port=2181
sr.hbase.super.user=hbase

```



##### MODELS

* Projemizde kullandığımız veri modeli yapıları.

```java
public class UserHBase  {

    private String uuid;

    private String name;

    private String surname;

    private int age;

    private String gender;
```



```java
@Entity
@Table(name = "user")
public class UserMySQL{
		
	@Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
	@Column(name="name")
    private String name;
    @Column(name = "surname")
    private String surname;
    @Column(name = "age")
    private int age;
    @Column(name = "gender")
    private String gender; 

         
}

```



***Not:*** MySQL ve Hbase veri modelleri farklı olduğundan iki farklı model kullanılmıştır. 



##### HBASE VERİ ÇEKME

***Not:*** Hbase konfigurasyonu BTM projesi ile aynı olduğundan tekrar açıklanmadı. 



* Hbase'den verileri çekerken üç metot kullandık: 
  * getAll: Tablodaki tüm verileri döner.
  * getRows: Tabloda ilgili satırı döner.
  * deleteRows: Tabloda ilgili satırı siler.

```java
@Repository
public interface HBaseDao<T> {

    public List<T> getAll (Connection connection, String tableName) throws IOException;
    public String getRows(Connection connection, String tableName, String rowkey);
    public void deleteRows(Connection connection,String tableName,String rowkey) throws IOException;

}
```

 

```java
@Service
public class HbaseUserRepository implements HBaseDao {
    

@Override
    public List<UserHBase> getAll(Connection connection, String tableName) throws IOException {
        byte[] Name = Bytes.toBytes("name");
        byte[] Surname = Bytes.toBytes("surname");
        byte[] Gender = Bytes.toBytes("gender");
        byte[] Age = Bytes.toBytes("age");
        byte[] CF_INFO = Bytes.toBytes("personal_info");
        List<UserHBase> userList= new ArrayList<>();
        Table table=connection.getTable(TableName.valueOf("user"));
        Scan scan1 =new Scan();
        ResultScanner scanner1=table.getScanner(scan1);



        for(Result res : scanner1) { //tüm satır ve sütunlardaki veriyi tarayıp getirecek
            String name=Bytes.toString(res.getValue(CF_INFO, Name));
            String surname =Bytes.toString(res.getValue(CF_INFO, Surname));
            String gender= Bytes.toString(res.getValue(CF_INFO, Gender));
            int age= Integer.valueOf(Bytes.toString(res.getValue(CF_INFO, Age)));
            String row = Bytes.toString(res.getRow());
            userList.add(new UserHBase(name,surname,age,gender,row));
            System.out.println(Bytes.toString(res.getRow()));
            System.out.println(name);
            System.out.println(surname);
            System.out.println(gender);
            System.out.println(age);

        }
            scanner1.close();


        System.out.println("End of getRows...");
        return userList;
    }

    @Override
    public String getRows(Connection connection, String tableName, String rowkey) {
        return null;
    }

    @Override
    public void deleteRows(Connection connection, String tableName, String rowkey) throws IOException {
        Table table=connection.getTable(TableName.valueOf(tableName));
        Delete delete = new Delete(Bytes.toBytes(rowkey));
            table.delete(delete);
    }
}
```



##### MYSQL CRUD OPERASYONLARI

* Eklediğimiz JPA bağımlılığı sayesinde Springboot CRUD operasyonlarını otomatik oluşturulmaktadır.

```java
@Repository
public interface UserDao extends JpaRepository<UserMySQL, Integer>{

}
```



* Repository'nin bize sağladığı metotları kullanarak MySQL veritabanına veri göndermeyi sağladık.

```java
@Service
public class UserManager implements UserService {

	
	private UserDao userDao;
	
	@Autowired
	public UserManager(UserDao userDao) {
		this.userDao=userDao;
	}
	
	@Override
	public List<UserMySQL> getAll() {
		
		return this.userDao.findAll();
	}

	@Override
	public UserMySQL getById(int id) {
		return this.userDao.getById(id);
	}

	@Override
	public void addUser(UserMySQL userMySQL) {
		this.userDao.save(userMySQL);
		
	}

}

```





##### RABBITMQ PRODUCER

***Not:*** BTM projesi ile aynı konfigurasyona sahip olduğundan tekrar eklenmemiştir.

```java
@Service
public class UserProducer {
    //Value ile application.properties de tanımlı konfigurasyon parametreleri kullanılır. 
    @Value("${sr.rabbit.routing.name}")
    private String routingName;
    @Value("${sr.rabbit.exchange.name}")
    private String exchangeName;

    
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    public UserProducer(RabbitTemplate rabbitTemplate){
        this.rabbitTemplate=rabbitTemplate;
    }

       public void sendToQueue(UserHBase user){
        System.out.println("Notification send id: "+user.getUuid());
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonstr = gson.toJson(user);
        rabbitTemplate.convertAndSend(exchangeName,routingName,jsonstr);

    }

}
```



* User producer aldığı user nesnesini GSON paketini kullanarak JSON objesine çevirir ve convertAndSend metodu ile  kuyruğa gönderir. 



##### MAIN CLASS

* Projemiz konsol tabanlı bir uygulama olduğundan main class'ta gerekli düzenlemeler yapılmıştır.

```java
@SpringBootApplication
@EnableJpaRepositories
public class SgwApplication implements CommandLineRunner {
 
   @Autowired
   private UserService userService;
   @Autowired
   private HBaseDao hBaseDao;
   @Autowired
   HBaseConfig hBaseConfig;
   @Autowired
   UserProducer userProducer;
   
	
    public static void main(String[] args) {
    	
        SpringApplication.run(SgwApplication.class, args);

        
    }
    @Override
    public void run(String... args) throws Exception {
     System.out.println("Running Spring Boot Application");
      
     
     List<UserHBase> usrList =new ArrayList<>();
     for(;;) {
    	 try {
             usrList=hBaseDao.getAll(hBaseConfig.createConnection(),"user");
         } catch (IOException e) {
             System.out.println(e.getMessage());;
         }for (UserHBase userHBase : usrList){
             UserMySQL user = new UserMySQL(userHBase.getName(),userHBase.getSurname(), userHBase.getAge(),userHBase.getGender());
             userService.addUser(user);
             userProducer.sendToQueue(userHBase);
             try {
                 hBaseDao.deleteRows(hBaseConfig.createConnection(), "user", userHBase.getUuid());
             }catch (Exception e)
             {
                 System.out.println(e.getMessage());
             }
             System.out.println(user.toString());

         }Thread.sleep(3000);

     }}}
```



* Uygulama çalışmaya başladığı zamanda ilk veriyi aldıktan sonra 3 saniye arayla Hbase tablosundan veri çekmeye devam etmektedir. 
* User service nesnesi MySQL operasyonları için oluşturulmuştur. MySQL'e veri göndermek için kullanılmaktadır. 
* Aktarılan tüm veriler Hbase tablosundan silinir.



