package com.argelastaj.centosoracle.controller;


import com.argelastaj.centosoracle.models.User;
import com.argelastaj.centosoracle.repsoitory.concretes.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/hello")
    public String hello() {
        return "success";
    }

    @GetMapping("/users")
    public ResponseEntity<List<User>> getUsers(@RequestParam(required = false) String name){
        try{
            List<User> users = new ArrayList<>();
            if(name == null)
            {
                userService.getAll().forEach(users::add);
            }
            else
                userService.getByNameContaining(name).forEach(users::add);
            if(users.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(users, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/user")
    public ResponseEntity<User> addUser(@RequestBody User user)throws IOException{
        User user1= this.userService.addUser(user);

            return new ResponseEntity<User>(user1, HttpStatus.OK);
        }

}
