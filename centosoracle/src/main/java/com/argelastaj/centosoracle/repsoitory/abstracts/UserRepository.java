package com.argelastaj.centosoracle.repsoitory.abstracts;

import com.argelastaj.centosoracle.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User,Integer> {
    List<User> findAll();
    List<User> findByNameContaining(String name);
    public User save(User user);
}
