package com.argelastaj.centosoracle.repsoitory.concretes;

import com.argelastaj.centosoracle.models.User;
import com.argelastaj.centosoracle.repsoitory.abstracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService{

    @Autowired
    UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getAll(){
        return this.userRepository.findAll();
    }
    public List<User> getByNameContaining(String name){
        return this.userRepository.findByNameContaining(name);
    }
    public User addUser(User user){
        return this.userRepository.save(user);
    }
}
