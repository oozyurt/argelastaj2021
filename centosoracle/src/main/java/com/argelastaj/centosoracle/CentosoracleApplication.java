package com.argelastaj.centosoracle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CentosoracleApplication {

    public static void main(String[] args) {
        SpringApplication.run(CentosoracleApplication.class, args);
    }

}
