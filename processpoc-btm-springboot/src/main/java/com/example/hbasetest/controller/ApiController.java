package com.example.hbasetest.controller;


import com.example.hbasetest.repository.HBaseHelper;
import com.example.hbasetest.hbase.config.Hbase;
import com.example.hbasetest.kafka.service.Producer;
import com.example.hbasetest.model.User;
import org.apache.hadoop.hbase.client.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;


@RestController
@RequestMapping("/api")
public class ApiController {

    private final Producer producer;
    @Autowired
    Hbase hbase;
    @Autowired
    public ApiController(Producer producer){
        this.producer=producer;
    }


    @GetMapping("/init")
   public ResponseEntity<String> inita() throws IOException {
        Hbase hbase =new Hbase();
        Connection connection = hbase.createConnection();
        HBaseHelper myBase=new HBaseHelper();
        myBase.deleteTable(connection,"user");
        myBase.createTable(connection,"user",new String[]{"personal_info"});

        return new ResponseEntity<String>("Success.", HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<String> save(@RequestBody User user)throws IOException{
        user.setUuid(UUID.randomUUID().toString());
        this.producer.sendInfo(user,"test_topic");
        return new ResponseEntity<String>("Success", HttpStatus.OK);

    }
    @GetMapping("/get/{row}")
    public ResponseEntity<String> get(@PathVariable String row) throws IOException {
        ArrayList<String> strings = new ArrayList<>();

        HBaseHelper helper =new HBaseHelper();

        return new ResponseEntity<String>(helper.getRows(hbase.createConnection()  ,"user",row),HttpStatus.OK);



    }




}
