package com.example.hbasetest.kafka.service;

import com.example.hbasetest.repository.HBaseHelper;
import com.example.hbasetest.hbase.config.Hbase;
import com.example.hbasetest.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class Consumer  {

    @Autowired
    Hbase hbase;
    HBaseHelper myBase = new HBaseHelper();

    //
    @KafkaListener(topics="test_topic", groupId="group_id") //listener; topiclerin değeri "test_topic" olan verileri okur
    public void consumeMessage(User user) throws IOException {

        myBase.putRows(hbase.createConnection(),"user",new String[]{user.getUuid()+":personal_info:name:"+user.getName(),user.getUuid()+":personal_info:surname:"+user.getSurname(),user.getUuid()+":personal_info:age:"+user.getAge(),
                user.getUuid()+":personal_info:gender:"+user.getGender()});

        //System.out.println("Topic: test_topic "+ myBase.getRows(hbase.createConnection(), "user",user.uuid));
    }

}
