package com.example.hbasetest.kafka.service;

import com.example.hbasetest.model.User;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.kafka.core.KafkaTemplate;

import org.springframework.stereotype.Service;

@Service
public class Producer {

    @Autowired
    private KafkaTemplate<String,Object> kafkaTemplate;


    public void sendInfo(User user, String topic){
        this.kafkaTemplate.send(topic,user);
    }

}
